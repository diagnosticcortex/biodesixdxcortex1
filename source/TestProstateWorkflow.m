%  This script tests the execution of the Prostate development and
%  validation workflows for the dxcortex.
%
clear;
format long;

% STEP 1 - Load Feature Table for development data
xlsxfile = '.\ExampleData\ProstateTestData\Realization_1.xlsx';
[~,~,devData] = xlsread(xlsxfile);
% Filename | Groupname | Definition | Ftrs ...
filenames = devData(2:end,1);
groupnames = devData(2:end,2);
definitions = double(strcmp(devData(2:end,3),ProstateWorkflow.group2));
ftrTable = cell2mat(devData(2:end,4:end));
ftrNames = devData(1,4:end);

% STEP 2 - Load Feature Table for validation data
xlsxfile = '.\ExampleData\ProstateTestData\Val_GSE10645.xlsx';
[~,~,valData] = xlsread(xlsxfile);
% Filename | Groupname | Definition | Ftrs ...
valFilenames = valData(2:end,1);
valGroupnames = valData(2:end,2);
valDefinitions = double(strcmp(valData(2:end,3),ProstateWorkflow.group2));
valFtrTable = cell2mat(valData(2:end,4:end));
valFtrNames = valData(1,4:end); % Better be the same as development!

% STEP 3 - Set up output files
outputDev = '.\Output\Prostate\ComparisonDev.xlsx';
outputVal = '.\Output\Prostate\ComparisonVal.xlsx';

% STEP 4 - Instantiate and call the prostate workflow
prostateWorkflow = ProstateWorkflow;
comparisonDev = prostateWorkflow.RunDevelopmentWorkflow(filenames, groupnames, definitions, ftrTable, ftrNames, outputDev);
comparisonVal = prostateWorkflow.RunValidationWorkflow(valFilenames, valGroupnames, valDefinitions, valFtrTable, outputVal);

