%  This script tests the execution of the Prostate development and
%  validation workflows for the dxcortex random forest comparison.
%
clear;
format long;
warning('off','all'); % We get warnings in the logistic regression
                      % this is handled.

% STEP 1 - Load Feature Table for development data
csvfile = '.\ExampleData\ProstateRFTestData\Realization_1_Trim500.csv';
devData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
filenames = table2array(devData(2:end,1));
groupnames = table2array(devData(2:end,2));
definitions = double(strcmp(table2array(devData(2:end,3)),ProstateWorkflow.group2));
ftrTable = table2array(devData(2:end,4:end));
ftrNames = table2array(devData(1,4:end));

% STEP 2 - Load Feature Table for validation data
csvfile = '.\ExampleData\ProstateRFTestData\Val_GSE10645_Additional500randomFtrs.csv';
valData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
valFilenames = table2array(valData(2:end,1));
valGroupnames = table2array(valData(2:end,2));
valDefinitions = double(strcmp(table2array(valData(2:end,3)),ProstateWorkflow.group2));
valFtrTable = table2array(valData(2:end,4:end));
valFtrNames = table2array(valData(1,4:end)); % Better be the same as development!

% STEP 3 - Set up output files
outputDev = '.\Output\ProstateRF\ComparisonDev.xlsx';
outputVal = '.\Output\ProstateRF\ComparisonVal.xlsx';

% STEP 4 - Instantiate and call the prostate workflow
prostateRFWorkflow = ProstateRFWorkflow;
comparisonDev = prostateRFWorkflow.RunDevelopmentWorkflow(filenames, groupnames, definitions, ftrTable, ftrNames, outputDev);
comparisonVal = prostateRFWorkflow.RunValidationWorkflow(valFilenames, valGroupnames, valDefinitions, valFtrTable, outputVal);

