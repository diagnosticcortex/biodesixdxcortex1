%  This script tests the execution of the NSCLC development and
%  validation workflows for the dxcortex.
%
clear;
format long;
warning('off','all'); % We get warnings in the logistic regression
                      % this is handled.

% STEP 1 - Load Feature Table for development data
csvfile = '.\ExampleData\NSCLCTestData\Realization_1.csv';
devData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
filenames = table2array(devData(2:end,1));
groupnames = table2array(devData(2:end,2));
groupnames2 = table2array(devData(2:end,3));
definitions = double(strcmp(table2array(devData(2:end,4)),NSCLCWorkflow.group2));
ftrTable = table2array(devData(2:end,20:end));
ftrNames = table2array(devData(1,20:end));

% STEP 2 - Load Feature Table for validation data
csvfile = '.\ExampleData\NSCLCTestData\GSE42127_Val.csv';
valData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
valFilenames = table2array(valData(2:end,1));
valGroupnames = table2array(valData(2:end,2));
valDefinitions = double(strcmp(table2array(valData(2:end,3)),NSCLCWorkflow.group2));
valFtrTable = table2array(valData(2:end,12:end));
valFtrNames = table2array(valData(1,12:end)); % Better be the same as development!

% STEP 3 - Set up output files
outputDev = '.\Output\NSCLC\ComparisonDev.xlsx';
outputVal = '.\Output\NSCLC\ComparisonVal.xlsx';

% STEP 4 - Instantiate and call the prostate workflow
nsclcWorkflow = NSCLCWorkflow;
comparisonDev = nsclcWorkflow.RunDevelopmentWorkflow(filenames, groupnames, groupnames2, definitions, ftrTable, ftrNames, outputDev);
comparisonVal = nsclcWorkflow.RunValidationWorkflow(valFilenames, valGroupnames, valDefinitions, valFtrTable, outputVal);

