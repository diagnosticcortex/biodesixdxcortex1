% This function creates a matrix of zeros and ones by applying the
% Rank KNN classifiers to the feature table.
function miniClassifications = ComputeRankKNNMiniClassifications(testFtrTable,trainingFtrTable, trainingDefs, miniClassifiers)
    % Predict all of the data through all of the mini-classifiers
    % to get a matrix of zeros and ones.  
    numTestSamples = size(testFtrTable,1);
    numMiniClass = length(miniClassifiers);
    
    miniClassifications = zeros(numTestSamples, numMiniClass);
    
    for i = 1:numMiniClass
       miniClass = miniClassifiers{i};
       for m=1:numTestSamples
           XToRank = [trainingFtrTable(:,miniClass.ftrs); testFtrTable(m,miniClass.ftrs)];
           rankedX = tiedrank(XToRank);
           trainingRankedX = rankedX(1:end-1,:);
           rankKNNClass = fitcknn(trainingRankedX,trainingDefs,'NumNeighbors',miniClass.k,'NSMethod','exhaustive','distance','euclidean');
           [labels,~,~] = predict(rankKNNClass,rankedX(end,:));
           miniClassifications(m,i) = labels(1);   
       end       
    end
end