function logRegCoeffs = LogisticRegressionWithDropout(miniClassifications, numericDefinitions, numDropoutIters, leaveInNumber)
%LOGISTICREGRESSIONWITHDROPOUT Performs logistic regression with dropout
%regularization.
%   Performs dropout iterations by randomly sampling the 
%   mini-classifiers and averages the coefficients over the dropout
%   iterations.

    % STEP 1 - Get the size of the problem
    numMiniClass = size(miniClassifications,2);
    
    % STEP 2 - Dropout iterations, solve logistic regression for each
    % iteration, adding the coefficients along the way, then dividing by
    % number of dropout iterations to get avg. coeffcients.
    logRegCoeffs = zeros(numMiniClass + 1,1);
    numSuccessfulIters = 0;
    for i=1:numDropoutIters
        randomMiniClass = datasample(1:numMiniClass, leaveInNumber, 'Replace', false);
        X = miniClassifications(:,randomMiniClass);        
        coeffs = LogRegNewtonRaphson(X,numericDefinitions);
        if (isnan(coeffs(1)))
            continue;
        end
        numSuccessfulIters = numSuccessfulIters + 1;
        logRegCoeffs(1) = logRegCoeffs(1) + coeffs(1);
        for j = 1:leaveInNumber
            logRegCoeffs(randomMiniClass(j)+1) = logRegCoeffs(randomMiniClass(j)+1) + coeffs(j+1);
        end
    end
    logRegCoeffs = logRegCoeffs / numSuccessfulIters;
end

