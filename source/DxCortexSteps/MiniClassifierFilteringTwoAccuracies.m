% Performs mini-classifier (KNN) filtering given accuracy criteria for 
% the training set and an external group.
function [passingMiniClassifiers, trainingFtrTable, trainingDefinitions] = MiniClassifierFilteringTwoAccuracies(filenames, groupnames, ... 
    numericDefinitions, ftrTable, ftrNames, useAllFeaturesAllLevels, ...
    maxFeaturesToUse, k, trainingGroup1, trainingGroup2, minAccuracyValue, ... 
    maxAccuracyValue, rowLogicalForAccuracy1, rowLogicalForAccuracy2)

   numFtrs = length(ftrNames);
      
   % To be consistent with Nextgen, always put training group 2 rows first
   % in the trainingFtrTable, then training group 1
   trainingGroup2Rows = strcmp(groupnames, trainingGroup2);
   trainingGroup1Rows = strcmp(groupnames, trainingGroup1);
   
   trainingFtrTable = [ftrTable(trainingGroup2Rows,:); ftrTable(trainingGroup1Rows,:)];     
   Y = [numericDefinitions(trainingGroup2Rows); numericDefinitions(trainingGroup1Rows)];
   trainingDefinitions = Y;
   
   count = 0;
   passingMiniClassifiers = {};
   passingLevel1 = [];
   for i=1:maxFeaturesToUse 
       % Try all combinations (without repeating) of size i from features
       combos = nchoosek(1:numFtrs, i);
       if useAllFeaturesAllLevels
           for j=1:length(combos)
               X = trainingFtrTable(:,combos(j,:));
               XTest1 = ftrTable(rowLogicalForAccuracy1,combos(j,:));
               XTest2 = ftrTable(rowLogicalForAccuracy2,combos(j,:));
               YTest1 = numericDefinitions(rowLogicalForAccuracy1);
               YTest2 = numericDefinitions(rowLogicalForAccuracy2);
               knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
               [labels1,~,~] = predict(knnClass,XTest1);
               [labels2,~,~] = predict(knnClass,XTest2);
               matches1 = labels1==YTest1;
               matches2 = labels2==YTest2;
               detectionRate1 = sum(matches1) / length(matches1);
               detectionRate2 = sum(matches2) / length(matches2);
               if (detectionRate1 >= minAccuracyValue(1) && detectionRate1 <= maxAccuracyValue(1)) && ...
                  (detectionRate2 >= minAccuracyValue(2) && detectionRate2 <= maxAccuracyValue(2))
                   count = count + 1;
                   passingMiniClass.ftrs = combos(j,:);
                   passingMiniClass.kNNClass = knnClass;                   
                   passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
               end
                   
           end
       else          
           if i == 1
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest1 = ftrTable(rowLogicalForAccuracy1,combos(j,:));
                   XTest2 = ftrTable(rowLogicalForAccuracy2,combos(j,:));
                   YTest1 = numericDefinitions(rowLogicalForAccuracy1);
                   YTest2 = numericDefinitions(rowLogicalForAccuracy2);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels1,~,~] = predict(knnClass,XTest1);
                   [labels2,~,~] = predict(knnClass,XTest2);
                   matches1 = labels1==YTest1;
                   matches2 = labels2==YTest2;
                   detectionRate1 = sum(matches1) / length(matches1);
                   detectionRate2 = sum(matches2) / length(matches2);
                   if (detectionRate1 >= minAccuracyValue(1) && detectionRate1 <= maxAccuracyValue(1)) && ...
                      (detectionRate2 >= minAccuracyValue(2) && detectionRate2 <= maxAccuracyValue(2))
                       count = count + 1;
                       passingLevel1 = [passingLevel1;combos(j,:)];
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           else
               combos = nchoosek(passingLevel1, i);
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest1 = ftrTable(rowLogicalForAccuracy1,combos(j,:));
                   XTest2 = ftrTable(rowLogicalForAccuracy2,combos(j,:));
                   YTest1 = numericDefinitions(rowLogicalForAccuracy1);
                   YTest2 = numericDefinitions(rowLogicalForAccuracy2);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels1,~,~] = predict(knnClass,XTest1);
                   [labels2,~,~] = predict(knnClass,XTest2);
                   matches1 = labels1==YTest1;
                   matches2 = labels2==YTest2;
                   detectionRate1 = sum(matches1) / length(matches1);
                   detectionRate2 = sum(matches2) / length(matches2);
                   if (detectionRate1 >= minAccuracyValue(1) && detectionRate1 <= maxAccuracyValue(1)) && ...
                      (detectionRate2 >= minAccuracyValue(2) && detectionRate2 <= maxAccuracyValue(2))
                       count = count + 1;
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           end
       end         
   end
end