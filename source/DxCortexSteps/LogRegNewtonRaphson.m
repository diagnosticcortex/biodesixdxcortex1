% Newton-Raphson solver for logistic regression.
function B = LogRegNewtonRaphson(X,y)
    xSize = size(X);
    XWithZerothCol = [ones(xSize(1),1) X];
    error = 1;
    stdError = ones(xSize(2) + 1, 1);
    iter = 0;
    maxIter = 200;
    errorTol = 1e-6;
    B = zeros(xSize(2) + 1, 1);
    while ((error > errorTol) && (iter < maxIter))
        iter = iter + 1;

        tmpD = XWithZerothCol * B;
        tmpDD = exp(tmpD);
        P = tmpDD ./ (1.0 + tmpDD);

        W = P.*(1-P);

        tmpYP = y - P;

        grad = XWithZerothCol' * tmpYP;

        xhat = W .* XWithZerothCol;

        infoM = XWithZerothCol' * xhat;

        lambda = 1e-5;

        infoMTrustReg = infoM + lambda*eye(size(infoM));

        invInfoMTrustReg = inv(infoMTrustReg);

        xDelta = invInfoMTrustReg * grad;
        stdError = sqrt(diag(invInfoMTrustReg));

        error = max(abs(xDelta));

        B = B + 0.1*xDelta;

    end

    % iter
    % error
    % stdError
end