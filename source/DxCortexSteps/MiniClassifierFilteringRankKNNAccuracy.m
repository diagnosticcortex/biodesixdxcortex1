% Performs mini-classifier (RankKNN) filtering given accuracy criteria.
function [passingMiniClassifiers, trainingFtrTable, trainingDefs] = MiniClassifierFilteringRankKNNAccuracy(filenames, groupnames, ... 
    numericDefinitions, ftrTable, ftrNames, useAllFeaturesAllLevels, ...
    maxFeaturesToUse, k, trainingGroup1, trainingGroup2, minAccuracyValue, ... 
    maxAccuracyValue, rowLogicalForAccuracy)

   numFtrs = length(ftrNames);
   
   % To be consistent with Nextgen, always put training group 2 rows first
   % in the trainingFtrTable, then training group 1
   trainingGroup2Rows = strcmp(groupnames, trainingGroup2);
   trainingGroup1Rows = strcmp(groupnames, trainingGroup1);
   
   trainingFtrTable = [ftrTable(trainingGroup2Rows,:); ftrTable(trainingGroup1Rows,:)];   
   
   trainingDefs = [numericDefinitions(trainingGroup2Rows); numericDefinitions(trainingGroup1Rows)];
   
   
   count = 0;
   passingMiniClassifiers = {};
   passingLevel1 = [];
   for i=1:maxFeaturesToUse 
       % Try all combinations (without repeating) of size i from features
       combos = nchoosek(1:numFtrs, i);
       if useAllFeaturesAllLevels
           for j=1:length(combos)
               X = trainingFtrTable(:,combos(j,:));
               XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
               YTest = numericDefinitions(rowLogicalForAccuracy);
               numTestSamplesForAccuracy = length(YTest);
               numCorrect = 0;
               for m=1:numTestSamplesForAccuracy
                   XToRank = [X; XTest(m,:)];
                   rankedX = tiedrank(XToRank);
                   trainingRankedX = rankedX(1:end-1,:);
                   rankKNNClass = fitcknn(trainingRankedX,trainingDefs,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels,~,~] = predict(rankKNNClass,rankedX(end,:));
                   if labels(1)==YTest(m)
                       numCorrect = numCorrect + 1;
                   end
               end
               
               detectionRate = numCorrect / numTestSamplesForAccuracy;
               
               if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                   count = count + 1;
                   passingMiniClass.ftrs = combos(j,:);   
                   passingMiniClass.k = k;
                   passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
               end
                   
           end
       else          
           if i == 1
               for j=1:length(combos)  
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   numTestSamplesForAccuracy = length(YTest);
                   numCorrect = 0;
                   for m=1:numTestSamplesForAccuracy
                       XToRank = [X; XTest(m,:)];
                       rankedX = tiedrank(XToRank);
                       trainingRankedX = rankedX(1:end-1,:);
                       rankKNNClass = fitcknn(trainingRankedX,trainingDefs,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                       [labels,~,~] = predict(rankKNNClass,rankedX(end,:));
                       if labels(1)==YTest(m)
                           numCorrect = numCorrect + 1;
                       end
                   end

                   detectionRate = numCorrect / numTestSamplesForAccuracy;

                   if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                       count = count + 1;
                       passingLevel1 = [passingLevel1;combos(j,:)];
                       passingMiniClass.k = k;
                       passingMiniClass.ftrs = combos(j,:);             
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           else
               combos = nchoosek(passingLevel1, i);
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   numTestSamplesForAccuracy = length(YTest);
                   numCorrect = 0;
                   for m=1:numTestSamplesForAccuracy
                       XToRank = [X; XTest(m,:)];
                       rankedX = tiedrank(XToRank);
                       trainingRankedX = rankedX(1:end-1,:);
                       rankKNNClass = fitcknn(trainingRankedX,trainingDefs,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                       [labels,~,~] = predict(rankKNNClass,rankedX(end,:));
                       if labels(1)==YTest(m)
                           numCorrect = numCorrect + 1;
                       end
                   end

                   detectionRate = numCorrect / numTestSamplesForAccuracy;

                   if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                       count = count + 1;
                       passingMiniClass.ftrs = combos(j,:);             
                       passingMiniClass.k = k;
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           end
       end         
   end
end