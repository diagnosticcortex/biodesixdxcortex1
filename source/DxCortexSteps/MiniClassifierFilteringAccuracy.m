% Performs mini-classifier (KNN) filtering given accuracy criteria.
function [passingMiniClassifiers, trainingFtrTable, trainingDefinitions] = MiniClassifierFilteringAccuracy(filenames, groupnames, ... 
    numericDefinitions, ftrTable, ftrNames, useAllFeaturesAllLevels, ...
    maxFeaturesToUse, k, trainingGroup1, trainingGroup2, minAccuracyValue, ... 
    maxAccuracyValue, rowLogicalForAccuracy)

   numFtrs = length(ftrNames);
      
   % To be consistent with Nextgen, always put training group 2 rows first
   % in the trainingFtrTable, then training group 1
   trainingGroup2Rows = strcmp(groupnames, trainingGroup2);
   trainingGroup1Rows = strcmp(groupnames, trainingGroup1);
   
   trainingFtrTable = [ftrTable(trainingGroup2Rows,:); ftrTable(trainingGroup1Rows,:)];     
   Y = [numericDefinitions(trainingGroup2Rows); numericDefinitions(trainingGroup1Rows)];
   trainingDefinitions = Y;
   
   count = 0;
   passingMiniClassifiers = {};
   passingLevel1 = [];
   for i=1:maxFeaturesToUse 
       % Try all combinations (without repeating) of size i from features
       combos = nchoosek(1:numFtrs, i);
       if useAllFeaturesAllLevels
           for j=1:length(combos)
               X = trainingFtrTable(:,combos(j,:));
               XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
               YTest = numericDefinitions(rowLogicalForAccuracy);
               knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
               [labels,~,~] = predict(knnClass,XTest);
               matches = labels==YTest;
               detectionRate = sum(matches) / length(matches);
               if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                   count = count + 1;
                   passingMiniClass.ftrs = combos(j,:);
                   passingMiniClass.kNNClass = knnClass;                   
                   passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
               end
                   
           end
       else          
           if i == 1
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels,~,~] = predict(knnClass,XTest);
                   matches = labels==YTest;
                   detectionRate = sum(matches) / length(matches);
                   if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                       count = count + 1;
                       passingLevel1 = [passingLevel1;combos(j,:)];
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           else
               combos = nchoosek(passingLevel1, i);
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels,~,~] = predict(knnClass,XTest);
                   matches = labels==YTest;
                   detectionRate = sum(matches) / length(matches);
                   if detectionRate >= minAccuracyValue && detectionRate <= maxAccuracyValue
                       count = count + 1;
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           end
       end         
   end
end