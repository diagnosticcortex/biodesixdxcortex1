% This function creates a matrix of zeros and ones by applying the
% KNN classifiers to the feature table.
function miniClassifications = ComputeKNNMiniClassifications(ftrTable,miniClassifiers)
    % Predict all of the data through all of the mini-classifiers
    % to get a matrix of zeros and ones.  
    numSamples = size(ftrTable,1);
    numMiniClass = length(miniClassifiers);
    
    miniClassifications = zeros(numSamples, numMiniClass);
    for i = 1:numMiniClass
        miniClass = miniClassifiers{i};
        XTest = ftrTable(:,miniClass.ftrs);
        [labels,score,cost] = predict(miniClass.kNNClass,XTest);
        
        miniClassifications(:,i) = labels;        
    end
end