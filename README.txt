Biodesix Diagnostic Cortex (DxCortex) 1

Contact Information - DxCortex@biodesix.com

LICENSE:
The matlab scripts and tools contained in this repository can be used in accordance with the New BSD license agreement provided in the 
License.txt file.

NOTE:
The DxCortex itself is implemented in C# and uses HPC to facilitate running large numbers of experiments with large data sets.
This repository contains Matlab code that demonstrates the execution of the DxCortex in the context of the experiments shown in 
"A Dropout-Regularized Classifier Development Approach Optimized for Precision Medicine Test Discovery from Omics Data".  The 
full data set to reproduce the results of the experiments shown in the paper is very large, so one instance of example data
is utilized for each of the three experiments.

The full original data sets are also provided in the FullData folder, with development subset data provided for the Prostate cases.

For more information, please contact the authors at DxCortex@biodesix.com.

SUPPLEMENTAL TOOLS:
Supplemental tools used to generate the random realizations and to analyze the results data are located in the Tools folder.  
The RealizationGenerator is written in C# (VS2012 Professional, .Net 4.5) and requires a valid Roguewave IMSL (6.5.0) license.
The ROC Curve generator is written in Matlab and consists of several matlab scripts, ROC.m is the main script.

OPERATIONAL EXECUTION:
This source code is written in Matlab and requires Matlab R2017a to run. An example is given for each of the three DxCortex workflows
used in the paper.  To run these, start Matlab, add the install location of the local copy of the repository and all subdirectories 
to the Matlab path, and change the current directory to be BiodesixDxCortex1 in Matlab.  These examples run a classifier development 
procedure for a given data set, and then run the classification of the provided validation sets.  Be sure to delete any existing
output data before executing the test scripts.

EXAMPLE 1: 10-Year Survival Prostate Cancer - Small Dataset Experiment
Run TestProstateWorkflow.m - Classifier Development is performed on one realization of one selection of 9 samples in each group,
then Classification is performed using the developed Dropout Regularized Combination Classifier on both the development and validation 
data sets. Output files are produced for each including output probabilities, output classification labels, and a 'y' or 'n' indicating
if the classification label matched (or did not match) the assigned definition for each sample.  This example takes 10's of minutes
to run.  

EXAMPLE 2: 10-Year Survival Prostate Cancer - Comparison to Other Methods
Run TestProstateRFWorkflow.m - Classifier Development is performed on one realization of one selection of 84 samples in each group,
using a trimmed feature table with 500 random features added (instead of 10,000 in the experiment shown in the paper).  This data 
set is trimmed to allow the example to run in a reasonable amount of time (~20 minutes). Classification is performed using the developed 
Dropout Regularized Combination Classifier on both the development and validation data sets with the validation data set also having 
500 random features added (instead of the 10,000 shown in the paper). Output files are produced for each including output 
probabilities, output classification labels, and a 'y' or 'n' indicating if the classification label matched (or did not match) 
the assigned definition for each sample.  A pair feature tables (development and validation) containing the full set of 10,000 random 
features is also located in the ExampleData directory, so the test script can be modified to use the full files.  CAUTION - If the full
files are used, the process takes many days to run. 

EXAMPLE 3: 4-Year Survival NSCLC - Confounding Effects 
Run TestNSCLCWorkflow.m - Classifier Development is performed on one realization of the development data set. Classification is 
performed using the developed Dropout Regularized Combination Classifier on both the development and validation data sets. 
Output files are produced for each including output probabilities, output classification labels, and a 'y' or 'n' indicating if the 
classification label matched (or did not match) the assigned definition for each sample.