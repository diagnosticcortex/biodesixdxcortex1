﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: MasterRealizationsGenerator.Program.cs
//
//      (C) Copyright 2014 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Conde Benoist   1.21.2014       Created the stand alone instance of this tool.
//      Conde Benoist   1.27.2014       Removed logic that limited the application to a single instance.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealizationsGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                RealizationsGeneratorUtils.LogUtils.InitializeLog();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new RealizationsGeneratorUI.RealizationsGeneratorDialog());
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Environment.Exit(500);
            }
        }
    }
}
