﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: LogUtils.Utilities.cs
//
//      (C) Copyright 2014 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Conde Benoist   1.21.2013      Created the stand alone instance of this tool.
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealizationsGeneratorUtils
{
    public static class LogUtils
    {
        
        #region InitializeLog
        public static void InitializeLog()
        {
            Central.Util.Log theLog = new Central.Util.Log();
            //get the appdata dir.
            string thePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            thePath = thePath + "\\RealizationsGenerator";
            //check for and create the RealizationsGenerator dir
            if (!System.IO.Directory.Exists(thePath))
            {
                System.IO.Directory.CreateDirectory(thePath);
            }

            // use the Init method with a path provided to change the log path
            theLog.Init(Central.Util.elogprofile.primary, thePath);
            theLog = null;
            Central.Util.Log.ProcessName = "RealizationsGenerator";
            Central.Util.Log.LogLevel = Central.Util.eloglevel.info;
            Central.Util.Log.LogWhere = Central.Util.elogwhere.file;
            Central.Util.Log.LogName = "RealizationsGeneratorLog";
            Central.Util.Log.LogQuotaFormat = Central.Util.elogquotaformat.no_restriction;
            Central.Util.Log.LogSizeMax = 1000;
            Central.Util.Log.LogPeriod = Central.Util.elogperiod.week;
            Central.Util.Log.LogNameFormat = Central.Util.elognameformat.name_date;
            Central.Util.Log.SetLogPath();
        }
        #endregion

    }
}
