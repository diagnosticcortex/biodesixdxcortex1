﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

///////////////////////////////////////////////////////////////////////////////
//
//      Filename: RealizationsGeneratorDialog.cs
//
//      (C) Copyright 2014 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Conde Benoist   1.3.2014        Created this file from original 'RealizationFileGenerator' by ER.
//      Conde Benoist   6.25.2014       Modifed the logic to use the Bflat DatatableUtils and TwoDimensionalArrayUtils method calls
//                                      and eliminate a local versions of these methods. Added method descriptions.
//
///////////////////////////////////////////////////////////////////////////////

namespace RealizationsGeneratorUI
{
    /// <summary>
    /// Dialog for selecting a source feature table along with controls for selecting the 
    /// training groups, numnber of training groups and the number of output realizations to create.
    /// </summary>
    public partial class RealizationsGeneratorDialog: Form
    {
        #region PrivateMembers
        /// <summary>
        /// varible that holds the type file type.
        /// </summary>
        private string _fileExtension = string.Empty;
        /// <summary>
        /// the source table used for creating the realizations
        /// </summary>
        private DataTable _masterFeatureTableDataTable = null;
        /// <summary>
        /// local instance of the temp file utils.
        /// </summary>
        private Central.Util.TempFileUtils _tmpfile = new Central.Util.TempFileUtils();
        /// <summary>
        /// temp file name for the last used file type.
        /// </summary>
        private string _tmpLastUsedFileType = "RealizationsGenerator.RealizationsGeneratorDialog.FileType";
        /// <summary>
        /// temp file name for the last used feature table
        /// </summary>
        private string _tmpLastUsedFeatureTable = "RealizationsGenerator.RealizationsGeneratorDialog.FeatureTable";
        /// <summary>
        /// temp file name for the last used feature table directory.
        /// </summary>
        private string _tmpLastUsedFeatureTableDir = "RealizationsGenerator.RealizationsGeneratorDialog.FeatureTableDir";
        /// <summary>
        /// temp file name for the last used worksheet
        /// </summary>
        private string _tmpLastUsedWorksheet = "RealizationsGenerator.RealizationsGeneratorDialog.Worksheet";
        /// <summary>
        /// temp file name for the last used Output folder.
        /// </summary>
        private string _tmpLastUsedOutputFolder = "RealizationsGenerator.RealizationsGeneratorDialog.OutputFolder";
        /// <summary>
        /// temp file name for the last used test group name.
        /// </summary>
        private string _tmpLastUsedTestGroupName = "RealizationsGenerator.RealizationsGeneratorDialog.TestGroup";
        /// <summary>
        /// temp file name for the last selected number of training group 1.
        /// </summary>
        private string _tmpLastUsedNumberGroup1 = "RealizationsGenerator.RealizationsGeneratorDialog.NumberGroup1";
        /// <summary>
        /// temp file name for the last selected number of training group 2.
        /// </summary>
        private string _tmpLastUsedNumberGroup2 = "RealizationsGenerator.RealizationsGeneratorDialog.NumberGroup2";
        /// <summary>
        /// temp file name for the last selected group column name.
        /// </summary>
        private string _tmpLastUsedSelectedGroupColumn = "RealizationsGenerator.RealizationsGeneratorDialog.SelectedGroupColumn";
        /// <summary>
        /// temp file name for the last selected Training group 1 from the group column value list.
        /// </summary>
        private string _tmpLastUsedTrainingGroup1 = "RealizationsGenerator.RealizationsGeneratorDialog.TrainingGroup1";
        /// <summary>
        /// temp file name for the last selected Training group 2 from the group column value list.
        /// </summary>
        private string _tmpLastUsedTrainingGroup2 = "RealizationsGenerator.RealizationsGeneratorDialog.TrainingGroup2";
        /// <summary>
        /// temp file name for the last used random seed value.
        /// </summary>
        private string _tmpLastUsedSeed = "RealizationsGenerator.RealizationsGeneratorDialog.Seed";
        /// <summary>
        /// temp file name for the last used number of realizations.
        /// </summary>
        private string _tmpLastUsedNumberOfRealizations = "RealizationsGenerator.RealizationsGeneratorDialog.NumberOfRealizations";
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public RealizationsGeneratorDialog()
        {
            try
            {                
                InitializeComponent();
                InitializeOwn();
            }
            catch (Exception ex)
            {
                Central.Util.ExceptionLogger.Log(ex);
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        private void InitializeOwn()
        {
            this.SetDialogIcon();
        }

        /// <summary>
        /// This method sets the dialog's controls to the last used values.
        /// </summary>
        private void InitializeLastUsedValues()
        {
            string filetype = this._tmpfile.GetString(this._tmpLastUsedFileType);
            if (!String.IsNullOrEmpty(filetype) && this.cmboBxFileType.Items.Contains(filetype))
            {
                this.cmboBxFileType.SelectedIndex = this.cmboBxFileType.Items.IndexOf(filetype);
            }

            string theMaster = this._tmpfile.GetString(this._tmpLastUsedFeatureTable);
            if (!String.IsNullOrEmpty(theMaster))
            {
                if (theMaster.EndsWith(filetype) && System.IO.File.Exists(theMaster))
                {
                    this.txtBxMasterFeatureTableFile.Text = theMaster;
                    this.UpdateUIWithMasterFeatureTable();
                }
            }

            string theOutputDir = this._tmpfile.GetString(this._tmpLastUsedOutputFolder);
            if (!String.IsNullOrEmpty(theOutputDir))
            {
                if (System.IO.Directory.Exists(theOutputDir))
                {
                    this.txtBxRealizationOutputDir.Text = theOutputDir;
                }
            }

            string theTestName = this._tmpfile.GetString(this._tmpLastUsedTestGroupName);
            if (!String.IsNullOrEmpty(theTestName))
            {
                this.txtBxTestGroupName.Text = theTestName;
            }


            string theNumberG1 = this._tmpfile.GetString(this._tmpLastUsedNumberGroup1);
            if (!String.IsNullOrEmpty(theNumberG1))
            {
                this.nudsTrainingGroup1PerRealization.Value = Convert.ToInt32(theNumberG1);
            }

            string theNumberG2 = this._tmpfile.GetString(this._tmpLastUsedNumberGroup2);
            if (!String.IsNullOrEmpty(theNumberG2))
            {
                this.nudsTrainingGroup2PerRealization.Value = Convert.ToInt32(theNumberG2);
            }

            this._tmpfile.SetString(
            this._tmpLastUsedSeed,
            this.txtbxRandomSeed.Text.ToString());

            string theSeed = this._tmpfile.GetString(this._tmpLastUsedSeed);
            if (!String.IsNullOrEmpty(theSeed))
            {
                this.txtbxRandomSeed.Text = theSeed;
            }

            string theNumberRealizations = this._tmpfile.GetString(this._tmpLastUsedNumberOfRealizations);
            if (!String.IsNullOrEmpty(theNumberRealizations))
            {
                this.txtbxNumberOfRealizations.Text = theNumberRealizations;
            }
        }


        /// <summary>
        /// Contructs a file browser dialog for selecting the source feature table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnBrowseMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.cmboBxFileType.Text))
                {
                    MessageBox.Show("Please select a file type.");
                    return;
                }

                string regex = "Master Classifier Files(*"+ this._fileExtension+")|*"+ this._fileExtension;
                string theFile = Central.Util.FileUtils.BrowseForFile(regex, this._tmpLastUsedFeatureTableDir);
                if (!String.IsNullOrEmpty(theFile))
                {
                    this.txtBxMasterFeatureTableFile.Text = theFile;
                    this._tmpfile.SetString(this._tmpLastUsedFeatureTable, theFile);
                    this.UpdateUIWithMasterFeatureTable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("sorry an unexpected error occured selecting the master feature table");
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        /// <summary>
        /// if an .xlsx file is selected, determines the worksheet names and populates the worksheet combobox.
        /// if a .csv file is selected, reads the content of the source file into the class table.
        /// </summary>
        private void UpdateUIWithMasterFeatureTable()
        {
            string theMasterFile = this.txtBxMasterFeatureTableFile.Text;
            if (!String.IsNullOrEmpty(theMasterFile))
            {
                if (theMasterFile.EndsWith(".xlsx"))
                {
                    string lastUsed = this._tmpfile.GetString(this._tmpLastUsedWorksheet);
                    int selectedIndex = 0;
                    string[] theWs = Central.Util.OpenXmlExcelReader.GetWorksheets(this.txtBxMasterFeatureTableFile.Text.ToString());
                    this.cmboBxSelectedWorksheet.Items.Clear();
                    int countem = 0;
                    foreach (string ws in theWs)
                    {
                        if (!String.IsNullOrEmpty(lastUsed))
                        {
                            if (lastUsed.Equals(ws))
                            {
                                selectedIndex = countem;
                            }
                        }
                        this.cmboBxSelectedWorksheet.Items.Add(ws);
                        countem++;
                    }
                    if (theWs.Length > 0)
                    {                        
                        this.cmboBxSelectedWorksheet.SelectedIndex = selectedIndex;
                    }
                    this.cmboBxSelectedWorksheet.Enabled = true;
                }
                else
                {
                    string[,] theCSVData =
                        Central.Util.TwoDimensionalStringArrayUtils.ReadCVSFile(this.txtBxMasterFeatureTableFile.Text);
                    this._masterFeatureTableDataTable =
                        Central.Util.TwoDimensionalStringArrayUtils.Convert2DStringToDataTable(theCSVData);
                    UpdateGroupSelections();
                    this.cmboBxSelectedWorksheet.Enabled = false;
                }
            }
        }

        /// <summary>
        /// when a file type is selected, updates the class varible and the temp file value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCmboBxFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._fileExtension = cmboBxFileType.SelectedItem.ToString();
            this._tmpfile.SetString(this._tmpLastUsedFileType, this._fileExtension);
        }

        /// <summary>
        /// Constructs a folder browser dialog for the destination realizations.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnBrowseTargetFolder_Click(object sender, EventArgs e)
        {
            try
            {
                string theInitDir = this._tmpfile.GetString(this._tmpLastUsedOutputFolder);
               
                if (String.IsNullOrEmpty(theInitDir))
                {
                    theInitDir = "C:";
                }

                string theDir = Central.Util.FileUtils.BrowseForDirectory(theInitDir, "Select output dir",true);
                if (!String.IsNullOrEmpty(theDir))
                {
                    this.txtBxRealizationOutputDir.Text = theDir;
                    this._tmpfile.SetString(this._tmpLastUsedOutputFolder, theDir);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("sorry an unexpected error occured selecting the output directory");
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        /// <summary>
        /// when a worksheet is seleted, reads the worsheet into the class table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCmboBxSelectedWorksheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    if (this._masterFeatureTableDataTable != null)
                    {
                        this._masterFeatureTableDataTable.Reset();
                        UpdateGroupSelections();
                    }
                    this.Refresh();
                    this.Cursor = Cursors.WaitCursor;
                    this._masterFeatureTableDataTable = 
                        Central.Util.OpenXmlExcelReader.GetDataTable(
                        this.txtBxMasterFeatureTableFile.Text,
                        this.cmboBxSelectedWorksheet.SelectedItem.ToString());

                    this._tmpfile.SetString(this._tmpLastUsedWorksheet, this.cmboBxSelectedWorksheet.SelectedItem.ToString());

                    UpdateGroupSelections();
                }
                catch (Exception ex1)
                {
                    if (ex1.Message.Contains("appears to be empty"))
                    {
                        return;
                    }
                    else
                    {
                        throw new Exception(ex1.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("sorry an unexpected error occured selecting the master feature table worksheet");
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// This method updates the contents of the group column combobox based on the worksheet selected. 
        /// sets the value if available and matches the saved value.
        /// to the last used.
        /// </summary>
        private void UpdateGroupSelections()
        {
            List<string> theColumns =
                Central.Util.DataTableUtils.GetColumnNames(this._masterFeatureTableDataTable);
            this.cmboBxGroupColumn.Items.Clear();
            foreach (string v in theColumns)
            {
                this.cmboBxGroupColumn.Items.Add(v);
            }
            string theGroup = this._tmpfile.GetString(this._tmpLastUsedSelectedGroupColumn);
            if (!String.IsNullOrEmpty(theGroup))
            {
                if (this.cmboBxGroupColumn.Items.Contains(theGroup))
                {
                    this.cmboBxGroupColumn.SelectedIndex = this.cmboBxGroupColumn.Items.IndexOf(theGroup);
                }
            }
        }

        /// <summary>
        /// when a group column is selected, fetches the distinct group names and updates the 
        /// training set list boxes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCmboBxGroupColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<string> theVals =
                    Central.Util.DataTableUtils.GetDistinctValuesForColumn(
                    this._masterFeatureTableDataTable,
                    this.cmboBxGroupColumn.SelectedItem.ToString());
               
                this.lstBxTrainingSetGroup1.Items.Clear();
                this.lstBxTrainingSetGroup2.Items.Clear();

                foreach (string v in theVals)
                {
                    this.lstBxTrainingSetGroup1.Items.Add(v);
                    this.lstBxTrainingSetGroup2.Items.Add(v);
                }

                String trainingGroup1 = this._tmpfile.GetString(this._tmpLastUsedTrainingGroup1);
                if (!String.IsNullOrEmpty(trainingGroup1))
                {
                    if (this.lstBxTrainingSetGroup1.Items.Contains(trainingGroup1))
                    {
                        this.lstBxTrainingSetGroup1.SelectedIndex = this.lstBxTrainingSetGroup1.Items.IndexOf(trainingGroup1);
                    }
                }

                String trainingGroup2 = this._tmpfile.GetString(this._tmpLastUsedTrainingGroup2);
                if (!String.IsNullOrEmpty(trainingGroup2))
                {
                    if (this.lstBxTrainingSetGroup2.Items.Contains(trainingGroup2))
                    {
                        this.lstBxTrainingSetGroup2.SelectedIndex = this.lstBxTrainingSetGroup2.Items.IndexOf(trainingGroup2);
                    }
                }
                    
                this._tmpfile.SetString(this._tmpLastUsedSelectedGroupColumn, 
                                        this.cmboBxGroupColumn.SelectedItem.ToString());                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Sorry, an unexpected error occured selecting the group column.");
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        /// <summary>
        /// verifies all required values are present for generating the output realizations.
        /// </summary>
        private void VerifyInputs()
        {
            if (String.IsNullOrEmpty(this.txtBxTestGroupName.Text))
            {
                throw new Exception ("empty test group name, please enter group name");
            }

            if (String.IsNullOrEmpty(this.txtBxMasterFeatureTableFile.Text))
            {
                throw new Exception("empty master feature table, please select master feature table");
            }

            if (String.IsNullOrEmpty(this.txtBxRealizationOutputDir.Text))
            {
                throw new Exception("empty output directory, please select output directory");
            }

            if (this.lstBxTrainingSetGroup1.SelectedItem == null)
            {
                throw new Exception("no training group1 selected, please select training group1");
            }
            if (this.lstBxTrainingSetGroup2.SelectedItem == null)
            {
                throw new Exception("no training group2 selected, please select training group1");
            }
            if (String.IsNullOrEmpty(this.txtbxNumberOfRealizations.Text))
            {
                throw new Exception("empty number realizations, please select number realizations");
            }
        }


        /// <summary>
        /// Generate the output files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnGenerateRealizations_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.VerifyInputs();

                int seed = 0;
                if(!Int32.TryParse(this.txtbxRandomSeed.Text, out seed))
                {
                    throw new Exception("Illegal seed, please verify and retry");
                }

                int number = 0;
                if (!Int32.TryParse(this.txtbxNumberOfRealizations.Text, out number))
                {
                    throw new Exception("Illegal number of realizations, please verify and retry");
                }
                
                Imsl.Stat.Random rand = new Imsl.Stat.Random(seed);
                string[,] orig =
                    Central.Util.TwoDimensionalStringArrayUtils.ConvertDataTableTo2DString(
                    this._masterFeatureTableDataTable);

                int groupCol = 
                    this.GetColumnNumberStartingAtZero(this._masterFeatureTableDataTable, this.cmboBxGroupColumn.SelectedItem.ToString());


                for (int i = 0; i < number; i++)
                {
                    string[,] realizationData = this.GetRealization(orig, groupCol, rand);
                    string filename = this.txtBxRealizationOutputDir.Text + "\\Realization_" + (i + 1).ToString() + this._fileExtension;
                    if (this._fileExtension.Equals(".csv"))
                    {
                        Central.Util.TwoDimensionalStringArrayUtils.WriteCSVFile(realizationData, filename);
                    }
                    else if (this._fileExtension.Equals(".xlsx"))
                    {                        
                        DataTable theDt =
                            Central.Util.TwoDimensionalStringArrayUtils.Convert2DStringToDataTable(realizationData);

                        Central.Util.OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(theDt, filename, "FeatureTable");
                        if (System.IO.File.Exists(filename))
                        {
                            string sheet1 = "Sheet1";
                            if (Central.Util.OpenXmlExcelReader.WorksheetExists(filename, sheet1))
                            {
                                Central.Util.OpenXmlExcelWriter.DeleteWorkSheet(filename, sheet1);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("invalid file extension please select file type and try again");
                    }
                }

                MessageBox.Show("Realization files generated successfully. Output in..\n\n " + 
                    this.txtBxRealizationOutputDir.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("sorry an unexpected error occured while generating realization data.");
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// gets the the column starting at 0.
        /// </summary>
        /// <param name="theDataTable"></param>
        /// <param name="theColumnName"></param>
        /// <returns></returns>
        private int GetColumnNumberStartingAtZero(DataTable theDataTable, string theColumnName)
        {
            int col = 0;
            foreach (DataColumn dc in theDataTable.Columns)
            {
                if (dc.ColumnName.Equals(theColumnName))
                {
                    return col;
                }
                col++;
            }
            return -1;
        }

        /// <summary>
        /// returns a realization table (2D array) for the supplied table (2D array) and the group column number
        /// <param name="orig">source feature table</param>
        /// <param name="groupCol">column containing group values</param>
        /// <param name="rand">Imsl random number generator</param>
        /// <returns></returns>
        private string[,] GetRealization(string[,] orig, int groupCol, Imsl.Stat.Random rand)
        {
            //make a copy of the original spreadsheet
            string[,] res = new string[orig.GetLength(0), orig.GetLength(1)];
            int n1 = 0;
            int n2 = 0;
            try
            {
                //14 for n1, and 18 for n2
                n1 = Convert.ToInt32(this.nudsTrainingGroup1PerRealization.Value);
                n2 = Convert.ToInt32(nudsTrainingGroup2PerRealization.Value);
            }
            catch (Exception ex)
            {
                throw new Exception("illegal value selected for number of training group1/group2 per realization");
            }

            if (String.IsNullOrEmpty(this.txtBxTestGroupName.Text))
            {
                throw new Exception("Empty test group name, please enter test group name");
            }
            
            double[] caser = new double[n1];
            for (int i = 0; i < n1; i++) caser[i] = rand.Next(12345678) / 12345678.0;


            string[] cases = new string[n1];
            for (int l = 0; l < n1 / 2; l++)
            {
                int i = 2 * l;
                if (caser[i] < caser[i + 1])
                {
                    //keep it as Early
                    cases[i] = this.lstBxTrainingSetGroup1.SelectedItem.ToString();
                    //make it Test
                    cases[i + 1] = this.txtBxTestGroupName.Text;
                }
                else
                {
                    //keep it early
                    cases[i + 1] = this.lstBxTrainingSetGroup1.SelectedItem.ToString();
                    //make it test
                    cases[i] = this.txtBxTestGroupName.Text;
                }
            }

          
            
            double[] casel = new double[n2];
            for (int i = 0; i < n2; i++) casel[i] = rand.Next(12345678) / 12345678.0;
            string[] casesl = new string[n2];

            //until l == 8
            for (int l = 0; l < n2 / 2; l++)
            {
                int i = 2 * l;
                if (casel[i] < casel[i + 1])
                {
                    casesl[i] = this.lstBxTrainingSetGroup2.SelectedItem.ToString();
                    casesl[i + 1] = this.txtBxTestGroupName.Text;
                }
                else
                {
                    casesl[i + 1] = this.lstBxTrainingSetGroup2.SelectedItem.ToString();
                    casesl[i] = this.txtBxTestGroupName.Text;
                }
            }           
           
            int nearly = n1;
            int nlate = n2;

            //copy it over by value;
            for (int i = 0; i < orig.GetLength(0); i++)
            {
                for (int j = 0; j < orig.GetLength(1); j++)
                {
                    res[i, j] = orig[i, j];
                }
            }

            int startRow =0;
            for (int i = 0; i < nearly; i++)
            {
                int rowIdx = GetNextRowWithGroupMatch(startRow, res, groupCol, this.lstBxTrainingSetGroup1.SelectedItem.ToString());
                res[rowIdx, groupCol] = cases[i];
                startRow = rowIdx+1;
            }
            startRow = 0;
            for (int i = 0; i < nlate; i++)
            {
                int rowIdx = GetNextRowWithGroupMatch(startRow, res, groupCol,this.lstBxTrainingSetGroup2.SelectedItem.ToString());
                res[rowIdx, groupCol] = casesl[i];
                startRow = rowIdx + 1;
            }
            return res;
        }

        private int GetNextRowWithGroupMatch(int startRow, string[,] res, int groupCol,
            string groupName)
        {
            if (startRow > res.GetLength(0) - 1)
            {
                throw new Exception("sorry, you cannot go past the end of the data, index " + startRow + " size " + res.GetLength(0));
            }
            for (int i = startRow; i < res.GetLength(0); i++)
            {
                if (res[i, groupCol].Equals(groupName))
                {
                    return i;
                }
            }
            throw new Exception("Sorry, could not find the group name " + groupName + " in the column number (starting at 0) " + groupCol);
        }



        /// <summary>
        /// set the last used temp file value for the training set group 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLstBxTrainingSetGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedTrainingGroup1, 
                this.lstBxTrainingSetGroup1.SelectedItem.ToString());
        }

        // <summary>
        /// set the last used temp file value for the training set group 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLstBxTrainingSetGroup2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedTrainingGroup2,
               this.lstBxTrainingSetGroup2.SelectedItem.ToString());
        }

        // <summary>
        /// set the last used temp file value for the group name/label for the new realizations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTxtBxTestGroupName_TextChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedTestGroupName,
              this.txtBxTestGroupName.Text);
        }

        // <summary>
        /// set the last used temp file value for the number of training group 1 labels to be assigned
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNudsTrainingGroup1PerRealization_ValueChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedNumberGroup1,
             this.nudsTrainingGroup1PerRealization.Value.ToString());
        }

        // <summary>
        /// set the last used temp file value for the number of training group 2 labels to be assigned
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNudsTrainingGroup2PerRealization_ValueChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedNumberGroup2,
             this.nudsTrainingGroup2PerRealization.Value.ToString());
        }

        // <summary>
        /// set the last used temp file value for the random seed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTxtBxRandomSeed_TextChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedSeed,
             this.txtbxRandomSeed.Text.ToString());
        }

        // <summary>
        /// set the last used temp file value for the number of realizations to generate.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTxtBxNumberOfRealizations_TextChanged(object sender, EventArgs e)
        {
            this._tmpfile.SetString(this._tmpLastUsedNumberOfRealizations,
             this.txtbxNumberOfRealizations.Text.ToString());
        }

        // <summary>
        /// close the dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region SetDialogIcon
        private void SetDialogIcon()
        {
            var icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            this.Icon = icon;
        }
        #endregion

        
        /// <summary>
        /// after the dialog is displed, update the last used values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RealizationsGeneratorDialog_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            this.InitializeLastUsedValues();
        }
    }
}
