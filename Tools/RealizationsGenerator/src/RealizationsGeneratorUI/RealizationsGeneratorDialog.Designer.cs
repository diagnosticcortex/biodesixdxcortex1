﻿namespace RealizationsGeneratorUI
{
    partial class RealizationsGeneratorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBxMasterFeatureTableFile = new System.Windows.Forms.TextBox();
            this.btnBrowseTargetFolder = new System.Windows.Forms.Button();
            this.txtBxRealizationOutputDir = new System.Windows.Forms.TextBox();
            this.btnBrowseMaster = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmboBxSelectedWorksheet = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmboBxFileType = new System.Windows.Forms.ComboBox();
            this.lstBxTrainingSetGroup1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lstBxTrainingSetGroup2 = new System.Windows.Forms.ListBox();
            this.txtBxTestGroupName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGenerateRealizations = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.nudsTrainingGroup2PerRealization = new System.Windows.Forms.NumericUpDown();
            this.nudsTrainingGroup1PerRealization = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtbxNumberOfRealizations = new System.Windows.Forms.TextBox();
            this.cmboBxGroupColumn = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbxRandomSeed = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudsTrainingGroup2PerRealization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudsTrainingGroup1PerRealization)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBxMasterFeatureTableFile
            // 
            this.txtBxMasterFeatureTableFile.Location = new System.Drawing.Point(11, 42);
            this.txtBxMasterFeatureTableFile.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxMasterFeatureTableFile.Name = "txtBxMasterFeatureTableFile";
            this.txtBxMasterFeatureTableFile.ReadOnly = true;
            this.txtBxMasterFeatureTableFile.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBxMasterFeatureTableFile.Size = new System.Drawing.Size(601, 20);
            this.txtBxMasterFeatureTableFile.TabIndex = 4;
            // 
            // btnBrowseTargetFolder
            // 
            this.btnBrowseTargetFolder.Location = new System.Drawing.Point(636, 118);
            this.btnBrowseTargetFolder.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseTargetFolder.Name = "btnBrowseTargetFolder";
            this.btnBrowseTargetFolder.Size = new System.Drawing.Size(63, 24);
            this.btnBrowseTargetFolder.TabIndex = 7;
            this.btnBrowseTargetFolder.Text = "Browse";
            this.btnBrowseTargetFolder.UseVisualStyleBackColor = true;
            this.btnBrowseTargetFolder.Click += new System.EventHandler(this.OnBtnBrowseTargetFolder_Click);
            // 
            // txtBxRealizationOutputDir
            // 
            this.txtBxRealizationOutputDir.Location = new System.Drawing.Point(11, 120);
            this.txtBxRealizationOutputDir.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxRealizationOutputDir.Name = "txtBxRealizationOutputDir";
            this.txtBxRealizationOutputDir.ReadOnly = true;
            this.txtBxRealizationOutputDir.Size = new System.Drawing.Size(601, 20);
            this.txtBxRealizationOutputDir.TabIndex = 6;
            // 
            // btnBrowseMaster
            // 
            this.btnBrowseMaster.Location = new System.Drawing.Point(636, 40);
            this.btnBrowseMaster.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseMaster.Name = "btnBrowseMaster";
            this.btnBrowseMaster.Size = new System.Drawing.Size(63, 24);
            this.btnBrowseMaster.TabIndex = 5;
            this.btnBrowseMaster.Text = "Browse";
            this.btnBrowseMaster.UseVisualStyleBackColor = true;
            this.btnBrowseMaster.Click += new System.EventHandler(this.OnBtnBrowseMaster_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cmboBxSelectedWorksheet);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtBxMasterFeatureTableFile);
            this.groupBox1.Controls.Add(this.btnBrowseMaster);
            this.groupBox1.Controls.Add(this.btnBrowseTargetFolder);
            this.groupBox1.Controls.Add(this.txtBxRealizationOutputDir);
            this.groupBox1.Location = new System.Drawing.Point(14, 74);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(716, 150);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Master Feature table, and output direcrtory";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 79);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Select Worksheet:";
            // 
            // cmboBxSelectedWorksheet
            // 
            this.cmboBxSelectedWorksheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboBxSelectedWorksheet.FormattingEnabled = true;
            this.cmboBxSelectedWorksheet.Location = new System.Drawing.Point(116, 75);
            this.cmboBxSelectedWorksheet.Margin = new System.Windows.Forms.Padding(2);
            this.cmboBxSelectedWorksheet.Name = "cmboBxSelectedWorksheet";
            this.cmboBxSelectedWorksheet.Size = new System.Drawing.Size(210, 21);
            this.cmboBxSelectedWorksheet.TabIndex = 31;
            this.cmboBxSelectedWorksheet.SelectedIndexChanged += new System.EventHandler(this.OnCmboBxSelectedWorksheet_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 104);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Output Folder: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Master Feature Table File: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "File Type:";
            // 
            // cmboBxFileType
            // 
            this.cmboBxFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboBxFileType.FormattingEnabled = true;
            this.cmboBxFileType.Items.AddRange(new object[] {
            ".xlsx",
            ".csv"});
            this.cmboBxFileType.Location = new System.Drawing.Point(84, 21);
            this.cmboBxFileType.Margin = new System.Windows.Forms.Padding(2);
            this.cmboBxFileType.Name = "cmboBxFileType";
            this.cmboBxFileType.Size = new System.Drawing.Size(138, 21);
            this.cmboBxFileType.TabIndex = 20;
            this.cmboBxFileType.SelectedIndexChanged += new System.EventHandler(this.OnCmboBxFileType_SelectedIndexChanged);
            // 
            // lstBxTrainingSetGroup1
            // 
            this.lstBxTrainingSetGroup1.FormattingEnabled = true;
            this.lstBxTrainingSetGroup1.Location = new System.Drawing.Point(22, 100);
            this.lstBxTrainingSetGroup1.Margin = new System.Windows.Forms.Padding(2);
            this.lstBxTrainingSetGroup1.Name = "lstBxTrainingSetGroup1";
            this.lstBxTrainingSetGroup1.Size = new System.Drawing.Size(116, 95);
            this.lstBxTrainingSetGroup1.TabIndex = 22;
            this.lstBxTrainingSetGroup1.SelectedIndexChanged += new System.EventHandler(this.OnLstBxTrainingSetGroup1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 81);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Training Set Group1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Training Set Group2";
            // 
            // lstBxTrainingSetGroup2
            // 
            this.lstBxTrainingSetGroup2.FormattingEnabled = true;
            this.lstBxTrainingSetGroup2.Location = new System.Drawing.Point(148, 100);
            this.lstBxTrainingSetGroup2.Margin = new System.Windows.Forms.Padding(2);
            this.lstBxTrainingSetGroup2.Name = "lstBxTrainingSetGroup2";
            this.lstBxTrainingSetGroup2.Size = new System.Drawing.Size(116, 95);
            this.lstBxTrainingSetGroup2.TabIndex = 24;
            this.lstBxTrainingSetGroup2.SelectedIndexChanged += new System.EventHandler(this.OnLstBxTrainingSetGroup2_SelectedIndexChanged);
            // 
            // txtBxTestGroupName
            // 
            this.txtBxTestGroupName.Location = new System.Drawing.Point(482, 41);
            this.txtBxTestGroupName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxTestGroupName.Name = "txtBxTestGroupName";
            this.txtBxTestGroupName.Size = new System.Drawing.Size(217, 20);
            this.txtBxTestGroupName.TabIndex = 26;
            this.txtBxTestGroupName.TextChanged += new System.EventHandler(this.OnTxtBxTestGroupName_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(384, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Test Group Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(388, 23);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(235, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "(label that will be used for generated test groups)";
            // 
            // btnGenerateRealizations
            // 
            this.btnGenerateRealizations.Location = new System.Drawing.Point(14, 461);
            this.btnGenerateRealizations.Margin = new System.Windows.Forms.Padding(2);
            this.btnGenerateRealizations.Name = "btnGenerateRealizations";
            this.btnGenerateRealizations.Size = new System.Drawing.Size(156, 24);
            this.btnGenerateRealizations.TabIndex = 29;
            this.btnGenerateRealizations.Text = "Generate Realizations";
            this.btnGenerateRealizations.UseVisualStyleBackColor = true;
            this.btnGenerateRealizations.Click += new System.EventHandler(this.OnBtnGenerateRealizations_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.nudsTrainingGroup2PerRealization);
            this.groupBox2.Controls.Add(this.nudsTrainingGroup1PerRealization);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtbxNumberOfRealizations);
            this.groupBox2.Controls.Add(this.cmboBxGroupColumn);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtbxRandomSeed);
            this.groupBox2.Controls.Add(this.lstBxTrainingSetGroup2);
            this.groupBox2.Controls.Add(this.lstBxTrainingSetGroup1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtBxTestGroupName);
            this.groupBox2.Location = new System.Drawing.Point(14, 243);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(716, 209);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Training Set Groups and output Test group, seed value, and number of reali" +
    "zations";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(319, 75);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(378, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Total # of spectra added to test group = ( # of Group1 ÷ 2) + ( # of Group2 ÷ 2)";
            // 
            // nudsTrainingGroup2PerRealization
            // 
            this.nudsTrainingGroup2PerRealization.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudsTrainingGroup2PerRealization.Location = new System.Drawing.Point(622, 121);
            this.nudsTrainingGroup2PerRealization.Margin = new System.Windows.Forms.Padding(2);
            this.nudsTrainingGroup2PerRealization.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudsTrainingGroup2PerRealization.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudsTrainingGroup2PerRealization.Name = "nudsTrainingGroup2PerRealization";
            this.nudsTrainingGroup2PerRealization.Size = new System.Drawing.Size(77, 20);
            this.nudsTrainingGroup2PerRealization.TabIndex = 36;
            this.nudsTrainingGroup2PerRealization.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudsTrainingGroup2PerRealization.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nudsTrainingGroup2PerRealization.ValueChanged += new System.EventHandler(this.OnNudsTrainingGroup2PerRealization_ValueChanged);
            // 
            // nudsTrainingGroup1PerRealization
            // 
            this.nudsTrainingGroup1PerRealization.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudsTrainingGroup1PerRealization.Location = new System.Drawing.Point(622, 96);
            this.nudsTrainingGroup1PerRealization.Margin = new System.Windows.Forms.Padding(2);
            this.nudsTrainingGroup1PerRealization.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudsTrainingGroup1PerRealization.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudsTrainingGroup1PerRealization.Name = "nudsTrainingGroup1PerRealization";
            this.nudsTrainingGroup1PerRealization.Size = new System.Drawing.Size(77, 20);
            this.nudsTrainingGroup1PerRealization.TabIndex = 35;
            this.nudsTrainingGroup1PerRealization.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudsTrainingGroup1PerRealization.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.nudsTrainingGroup1PerRealization.ValueChanged += new System.EventHandler(this.OnNudsTrainingGroup1PerRealization_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(419, 100);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(181, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "# of Training Group 1 per Realization";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(526, 179);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "# Realizations";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 45);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Group Column:";
            // 
            // txtbxNumberOfRealizations
            // 
            this.txtbxNumberOfRealizations.Location = new System.Drawing.Point(622, 175);
            this.txtbxNumberOfRealizations.Margin = new System.Windows.Forms.Padding(2);
            this.txtbxNumberOfRealizations.Name = "txtbxNumberOfRealizations";
            this.txtbxNumberOfRealizations.Size = new System.Drawing.Size(77, 20);
            this.txtbxNumberOfRealizations.TabIndex = 10;
            this.txtbxNumberOfRealizations.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbxNumberOfRealizations.TextChanged += new System.EventHandler(this.OnTxtBxNumberOfRealizations_TextChanged);
            // 
            // cmboBxGroupColumn
            // 
            this.cmboBxGroupColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboBxGroupColumn.FormattingEnabled = true;
            this.cmboBxGroupColumn.Location = new System.Drawing.Point(119, 41);
            this.cmboBxGroupColumn.Margin = new System.Windows.Forms.Padding(2);
            this.cmboBxGroupColumn.Name = "cmboBxGroupColumn";
            this.cmboBxGroupColumn.Size = new System.Drawing.Size(145, 21);
            this.cmboBxGroupColumn.TabIndex = 29;
            this.cmboBxGroupColumn.SelectedIndexChanged += new System.EventHandler(this.OnCmboBxGroupColumn_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(525, 153);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Random Seed";
            // 
            // txtbxRandomSeed
            // 
            this.txtbxRandomSeed.Location = new System.Drawing.Point(622, 149);
            this.txtbxRandomSeed.Margin = new System.Windows.Forms.Padding(2);
            this.txtbxRandomSeed.Name = "txtbxRandomSeed";
            this.txtbxRandomSeed.Size = new System.Drawing.Size(77, 20);
            this.txtbxRandomSeed.TabIndex = 8;
            this.txtbxRandomSeed.Text = "123456";
            this.txtbxRandomSeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbxRandomSeed.TextChanged += new System.EventHandler(this.OnTxtBxRandomSeed_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmboBxFileType);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(14, 10);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(716, 51);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Select the file type of the master feature table (csv, or xlsx)";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(655, 460);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 32;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.OnBtnClose_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(419, 125);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(181, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "# of Training Group 2 per Realization";
            // 
            // RealizationsGeneratorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 495);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnGenerateRealizations);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(760, 533);
            this.Name = "RealizationsGeneratorDialog";
            this.Text = "Realizations Generator";
            this.Shown += new System.EventHandler(this.RealizationsGeneratorDialog_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudsTrainingGroup2PerRealization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudsTrainingGroup1PerRealization)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtBxMasterFeatureTableFile;
        private System.Windows.Forms.Button btnBrowseTargetFolder;
        private System.Windows.Forms.TextBox txtBxRealizationOutputDir;
        private System.Windows.Forms.Button btnBrowseMaster;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmboBxFileType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lstBxTrainingSetGroup1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstBxTrainingSetGroup2;
        private System.Windows.Forms.TextBox txtBxTestGroupName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGenerateRealizations;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtbxNumberOfRealizations;
        private System.Windows.Forms.ComboBox cmboBxGroupColumn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbxRandomSeed;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmboBxSelectedWorksheet;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudsTrainingGroup1PerRealization;
        private System.Windows.Forms.NumericUpDown nudsTrainingGroup2PerRealization;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label13;
    }
}