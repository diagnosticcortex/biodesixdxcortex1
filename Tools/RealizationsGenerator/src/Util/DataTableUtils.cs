﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//////////////////////////////////////////////////////////////////////////////
//
//      Filename: DataTableUtils.cs
//
//      (C) Copyright 2014 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ed Rotthoff     6/9/2014        created this class
//      Conde Benoist   6.30.2014       Integrated this class into Bflat.Utils lib. Modified the CompareDataRowCompareFieldsAsInt
//                                      method to accept doubles if the value can be parsed as an integer. If the value is unable to
//                                      parse as an integer, an exception is thrown.
//
///////////////////////////////////////////////////////////////////////////////
namespace Central.Util
{
    /// <summary>
    /// this class contains methods that can be used to process, and sort DataTables.
    /// </summary>
    public static class DataTableUtils
    {
        /// <summary>
        /// this method returns the list of column names within the data table
        /// </summary>
        /// <param name="theDataTable">the data table to process</param>
        /// <returns>the list of column names within the specified data table.</returns>
        public static List<string> GetColumnNames(DataTable theDataTable)
        {
            List<string> distinctValues = new List<string>();

            foreach (DataColumn dc in theDataTable.Columns)
            {
                distinctValues.Add(dc.ColumnName);
            }
            return distinctValues;
        }

        /// <summary>
        /// this method returns the list of distinct values that are present in within the specified
        /// column of the specified data table
        /// </summary>
        /// <param name="theDataTable">the data table to process</param>
        /// <param name="columnName">the column name for which to get the distinct values</param>
        /// <returns>the list of distinct values within the specified column</returns>
        public static List<string> GetDistinctValuesForColumn(DataTable theDataTable, string columnName)
        {
            List<string> distinctValues = new List<string>();

            var result = (from row in theDataTable.AsEnumerable()
                          where !(String.IsNullOrEmpty(row.Field<string>(columnName)))
                          select (string)(row.Field<string>(columnName)));

            if (result != null)
            {
                List<string> theResults = new List<string>(result);
                if (theResults.Count == 0)
                {
                    throw new Exception("Error Reading the unique values. no unique values found.");
                }


                foreach (string s in theResults)
                {
                    string theVal = s;
                    if ((theVal != null) && (theVal.Length > 0))
                    {
                        if (!(distinctValues.Contains(theVal)))
                        {
                            distinctValues.Add(theVal);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("select distinct values failed");
            }
            return distinctValues;
        }

        //the field to use in the comparer when sorting data rows.
        private static string _compareField = string.Empty;
        /// <summary>
        /// this method will set the compare field to be the specified column within the data tabel
        /// </summary>
        /// <param name="value">the column name to compare</param>
        public static void SetCompareField(string value)
        {
            _compareField = value;
        }

        /// <summary>
        /// this method will compare the 2 specified data rows, it will compare using the 
        /// compare field, or column specified within the SetCompareField method.
        /// </summary>
        /// <param name="x">the first datarow</param>
        /// <param name="y">the second data row</param>
        /// <returns>less than zero, means x less than y  equals 0 means x equals y greater than zero means x greater than y</returns>
        public static int CompareDataRowCompareFieldsAsInt(DataRow x, DataRow y)
        {
            int result = 0;

            if (x == null)
            {
                // If x is null and y is not null, y is greater
                if (y != null)
                {
                    result = -1;
                }
            }
            else
            {
                // If x is not null and y is null, x is greater
                if (y == null)
                {
                    result = 1;
                }
                else
                {

                    int xOrder = 0;
                    int yOrder = 0;
                    int orderSort = 0;

                    // Get Orders
                    if (!Int32.TryParse(x[_compareField].ToString(), out xOrder))
                    {
                        Double dVal = 0.0;
                        if (Double.TryParse(x[_compareField].ToString(), out dVal))
                        {
                            int truncVal = (int)dVal;
                            if ((Double)truncVal == dVal)
                            {
                                xOrder = truncVal;
                            }
                            else
                            {
                                throw new Exception("A value in '" + _compareField + "' column cannot be parsed as an integer.");
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid data found in '" + _compareField + "' column. Cannot be parsed as a number.");
                        }
                    }
                    if (!Int32.TryParse(y[_compareField].ToString(), out yOrder))
                    {
                        Double dVal = 0.0;
                        if (Double.TryParse(y[_compareField].ToString(), out dVal))
                        {
                            int truncVal = (int)dVal;
                            if ((Double)truncVal == dVal)
                            {
                                yOrder = truncVal;
                            }
                            else
                            {
                                throw new Exception("A value in '" + _compareField + "' column cannot be parsed as an integer.");
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid data found in '" + _compareField + "' column. Cannot be parsed as a number.");
                        }
                    }

                    orderSort = xOrder.CompareTo(yOrder);
                    result = orderSort;
                }
            }

            return result;
        }


        /// <summary>
        /// this method will compare the 2 specified data rows, it will compare using the 
        /// compare field, or column specified within the SetCompareField method.
        /// </summary>
        /// <param name="x">the first datarow</param>
        /// <param name="y">the second data row</param>
        /// <returns>less than zero, means x less than y  equals 0 means x equals y greater than zero means x greater than y</returns>
        public static int CompareDataRowCompareFieldsAsDouble(DataRow x, DataRow y)
        {
            int result = 0;

            if (x == null)
            {
                // If x is null and y is not null, y is greater
                if (y != null)
                {
                    result = -1;
                }
            }
            else
            {
                // If x is not null and y is null, x is greater
                if (y == null)
                {
                    result = 1;
                }
                else
                {

                    double xOrder = 0;
                    double yOrder = 0;
                    int orderSort = 0;

                    // Get Orders
                    if (!Double.TryParse(x[_compareField].ToString(), out xOrder))
                    {
                            throw new Exception("Invalid data found in '" + _compareField + "' column. Cannot be parsed as a number.");
                    }
                    if (!Double.TryParse(y[_compareField].ToString(), out yOrder))
                    {
                            throw new Exception("Invalid data found in '" + _compareField + "' column. Cannot be parsed as a number.");
                    }

                    orderSort = xOrder.CompareTo(yOrder);

                    result = orderSort;
                }
            }

            return result;
        }
    }
}
