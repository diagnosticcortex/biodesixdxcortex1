using System;
using System.Collections.Generic;
using System.Text;

namespace Central.Util
{
    public class SettingsUtil
    {
        public static string GetLabelForFilename(SortedDictionary<string, List<string>> fileTable,
            string filename)
        {
            foreach (string label in fileTable.Keys)
            {
                foreach (string fnm in fileTable[label])
                {
                    if (fnm.Trim().Equals(filename.Trim()))
                    {
                        return label;
                    }
                }
            }
            return null;
        }

       
    }
}
