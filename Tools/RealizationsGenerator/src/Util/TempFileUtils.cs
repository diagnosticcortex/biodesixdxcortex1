///////////////////////////////////////////////////////////////////////////////
//
//      Filename: TempFileUtils.cs
//
//      (C) Copyright 2011 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Conde Benoist   02.23.2012      Fixed bugs caused by static variables.
//      Ed Rotthoff     01/16/2013      Ensure the the class works without the log being initialized.
//      Ben Linstid     02/12/2013      Added logic to create the temp file if it does not exist.
//      Ed Rotthoff     6/11/2014       migrate to using the roaming directory. add lots of logic so it remains robust.
//
//////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Collections;
using Microsoft.Win32;

namespace Central.Util
{
	/// <summary>
	/// This class provides a utility class that reads and writes values specified by the user
    /// to the AnalysisCentraltmp.txt file in the user\AppData\Roaming directory. Use this
    /// class to remember last used values within your applications.
	/// </summary>
	public class TempFileUtils
	{
		private static string tmpFileName = "AnalysisCentraltmp.txt";
        private static string tmpFileSubDir = "\\Biodesix";
		private string path = null;
		private string fullfilepath = null;
		private System.Collections.Hashtable ht = new Hashtable(10);

		private void load(string fullfilepath)
		{
			StreamReader sr= null;
			try
			{
                // Check existence, if it is not there, create the temp file.
                if (!File.Exists(fullfilepath))
                {
                    FileStream fs = File.Create(fullfilepath, 10);
                    fs.Close();
                }
				ht.Clear();
				sr = new StreamReader(fullfilepath);
				String s = sr.ReadLine();
				while(s!= null)
				{
			
					//format of the line is "key":"; delimeted value string"
					int keyidx = s.IndexOf(":");
					string key = s.Substring(0,keyidx);
					string val = s.Substring(keyidx+1,s.Length-(keyidx+1));
					ht.Add(key,val);
					s=sr.ReadLine();
				}
			}
			catch(Exception e)
			{
                ExceptionLogger.Log(e);
                throw new Exception("TempFileUtils: Error loading or creating the file " + fullfilepath);
			}
			finally
			{
				if (sr != null)
				{
					sr.Close();
				}
			}

		}
		private void store(string fullfilepath)
		{
			StreamWriter sw = null;
			try
			{
				sw = new StreamWriter(fullfilepath,false);
				foreach(Object o in ht.Keys)
				{
			
					string key = (string)o;
					string val = (string)ht[key];
					//format of the line is "key":"; delimeted value string"
					string writestr = key+":"+val;
					sw.WriteLine(writestr);
				}
			}
			catch(Exception e)
			{
                ExceptionLogger.Log(e);
                throw new Exception("File Store Error for file "+fullfilepath);
			}
			finally
			{
				if (sw != null)
				{
					sw.Close();
				}
			}


		}

        /// <summary>
        /// this is the default constructor, if this is used
        /// then the tmp file will exist under the present users
        /// user directory (typically the UserName\AppData\Roaming\Biodesix directory).
        /// </summary>
        public TempFileUtils()
        {            
            //check for the old location, we will migrate to using the Roaming dir,
            //but dont want to lose any previously used settings.
		    string tmppath = Path.GetTempPath();
		    string tmpfullfilepath = tmppath+tmpFileName;
            //get the new location, roaming\Biodesix
            path = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            path += tmpFileSubDir;
            fullfilepath = path + "\\" + tmpFileName;

            //if the biodesix dir does not exist then create it.
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
       
            //if the file exists in the old temp area
            if (File.Exists(tmpfullfilepath))
            {
                //if the file does not exist in the new area
                if (!File.Exists(fullfilepath))
                {
                    //migrate to the non temp dir.        
                    File.Move(tmpfullfilepath, fullfilepath);
                }
            }
          
        }
        /// <summary>
        /// this constructor specifies the path to use for the 
        /// AnalysisCentraltmp.txt files location. the file
        /// will be accessed from this location.
        /// The path will be created if it does not exist.
        /// </summary>
        /// <param name="pathToUse"></param>
        public TempFileUtils(string pathToUse)
        {
            path = pathToUse;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            fullfilepath = path + "\\"+ tmpFileName;
        }

		/// <summary>
		/// Get a string from a key
		/// </summary>
		/// <param name="name">Value to look up within the key</param>
		/// <returns>value found</returns>
		public String GetString(string name)
		{
            String s = null;
            try
            {
                this.load(fullfilepath);
                s = (String)this.ht[name];
            }
            catch (Exception ex)
            {
                ExceptionLogger.Log(ex);
                return null;
            }
           
			return s;
		}

		/// <summary>
		/// Seperator character for storing multiple strings within a
		/// single registry value
		/// </summary>
		private const char STRING_SEPERATOR = ';';

		/// <summary>
		/// Get an array of strings from a key
		/// </summary>
		/// <param name="name">Value to look up within the key</param>
		/// <returns>Array of values</returns>
		public ArrayList GetStrings(string name)
		{
            ArrayList arr = new ArrayList();
            try
            {
                this.load(fullfilepath);
                String s = (String)this.ht[name];
               
                while (s != null)
                {
                    int pos = s.IndexOf(STRING_SEPERATOR);
                    if (pos != -1)
                    {
                        arr.Add(s.Substring(0, pos));
                        s = s.Substring(pos + 1);
                    }
                    else
                    {
                        arr.Add(s);
                        s = null;
                    }
                }
            }
            catch (Exception Ex)
            {
                return new ArrayList();
            }
			return arr;
		}

		/// <summary>
		/// Set a string to a key
		/// </summary>
		/// <param name="name">Value to look up within the key</param>
		/// <param name="value">Value to store</param>
		public void SetString(string name, String value)
		{
			this.load(fullfilepath);
			if(ht.ContainsKey(name))
				ht.Remove(name);

			ht.Add(name, value);
			this.store(fullfilepath);
		}

		/// <summary>
		/// Set an array of strings to a key
		/// </summary>
		/// <param name="name">Value to look up within the key</param>
		/// <param name="arr">Arra of strings to store</param>
		public void SetStrings(string name, ArrayList arr)
		{
			this.load(fullfilepath);
			string combined = "";
			foreach (object s in arr)
			{
				if (combined.Length > 0) combined += STRING_SEPERATOR;
				combined += s.ToString();
			}
			if(ht.ContainsKey(name))
				ht.Remove(name);

			ht.Add(name, combined);
			this.store(fullfilepath);
		}

	}
}
