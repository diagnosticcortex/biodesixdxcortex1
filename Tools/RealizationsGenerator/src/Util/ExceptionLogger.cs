using System;
using System.Collections.Generic;
using System.Text;
///////////////////////////////////////////////////////////////////////////////
//
//      Filename: ExceptionLogger.cs
//
//      (C) Copyright 2011 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ed Rotthoff     01/16/2013      Ensure the the class works without the log being initialized.
//
//////////////////////////////////////////////////////////////////////////////
namespace Central.Util
{
    /// <summary>
    /// this class will log exceptions.
    /// </summary>
    public static class ExceptionLogger
    {
        /// <summary>
        /// this method will log the exception to the Central.Util.Log
        /// if the log is not initialized, no logging will occur
        /// </summary>
        /// <param name="ex">the exception to log</param>
        public static void Log(Exception ex)
        {
            if (Central.Util.Log.IsInit)
            {
                Central.Util.Log.LogThis("ERROR --  ", eloglevel.error);
                Central.Util.Log.LogThis("          " + ex.Message, eloglevel.error);
                Central.Util.Log.LogThis("          " + ex.StackTrace, eloglevel.error);
            }
        }
    }
}
