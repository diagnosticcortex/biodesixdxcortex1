﻿//////////////////////////////////////////////////////////////////////////////
//
//      Filename: TwoDimensionalStringArrayUtils.cs
//
//      (C) Copyright 2014 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ed Rotthoff    6/9/2014         create the stratified realizations generator tool.
//      Ben Linstid    02/24/2015      Close the StreamReader instance in ReadCVSFile.
//      Ben Linstid    01/28/2016      Added methods to read/write 2D double arrays, 1D double array, 1D int array
//      Ed Rotthoff    9/16/2016       Added more detailed error messages when filename length errors occur.
//      Ben Linstid    09/01/2017      Improved the performance of the writecsv methods.
//      Ben Linstid    03/26/2018      Added method to write a string[] to csv.
//
///////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Central.Util
{
    /// <summary>
    /// this class contains utility methods relating to 2d arrays of strings.
    /// 
    /// There are a host of CSV file utilities in this class that should be moved to a common
    /// CSV utilty class at some point!
    /// </summary>
    public static class TwoDimensionalStringArrayUtils
    {
        /// <summary>
        /// this method will convert the specified data table into a 2d array of strings.
        /// the 0th row contains the column headers.
        /// </summary>
        /// <param name="theData">the data table</param>
        /// <returns>a 2d array of strings, the oth row of the array contains the column headers</returns>
        public static string[,] ConvertDataTableTo2DString(DataTable theData)
        {
            string[,] thestr = new string[theData.Rows.Count + 1, theData.Columns.Count];
            int row = 0;
            int column = 0;
            foreach (DataColumn dc in theData.Columns)
            {
                thestr[row, column] = dc.ColumnName;
                column++;
            }
            row++;
            foreach (DataRow dr in theData.Rows)
            {
                column = 0;
                foreach (DataColumn dc in theData.Columns)
                {
                    thestr[row, column] = dr[dc.ColumnName].ToString();
                    column++;
                }
                row++;
            }
            return thestr;
        }
        /// <summary>
        /// this method will convert a 3d array of strings to a datatable,
        /// the oth row is used as the column headers
        /// </summary>
        /// <param name="theData">the 2d array of strings</param>
        /// <returns>a datatable populated from the 2d array of strings, the 0th row of string data is used as the column headers</returns>
        public static DataTable Convert2DStringToDataTable(string[,] theData)
        {
            DataTable theTable = new DataTable();
            List<string> colNames = new List<string>();
            int row = 0;
            for (int col = 0; col < theData.GetLength(1); col++)
            {
                theTable.Columns.Add(theData[row, col], typeof(string));
                colNames.Add(theData[row, col]);
            }
            row++;
            for (int r = row; r < theData.GetLength(0); r++)
            {
                DataRow newRow = theTable.NewRow();

                for (int col = 0; col < theData.GetLength(1); col++)
                {
                    newRow[colNames[col]] = theData[r, col];
                }
                theTable.Rows.Add(newRow);
            }
            return theTable;
        }

        /// <summary>
        /// This method will read a comma separated file into a 2d array of strings.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 2d array of strings populated from the comma separated file.</returns>
        public static string[,] ReadCVSFile(string filename)
        {
            List<List<string>> theData = new List<List<string>>();
            StreamReader sr = null;



            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = null;
                int numberColumns = -1;

                while ((line = sr.ReadLine()) != null)
                {

                    string[] items = line.Split(',');
                    if (numberColumns == -1)
                    {
                        numberColumns = items.Count();
                    }
                    if (items.Count() != numberColumns)
                    {
                        throw new Exception("the data file " + filename + " contains a variable number of columns within its data, this cannot be read");
                    }

                    List<string> theValues = new List<string>();
                    for (int l = 0; l < items.Count(); l++)
                    {
                        theValues.Add(items[l]);
                    }
                    theData.Add(theValues);
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }
            int rows = theData.Count;
            int columns = theData[0].Count;
            string[,] theRet = new string[rows, columns];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    theRet[r, c] = theData[r][c];
                }
            }

            return theRet;
        }
        /// <summary>
        /// This method will populate a comma seperated file from the specified 2d array of strings.
        /// </summary>
        /// <param name="data">The 2d array of strings to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(string[,] data, string filename)
        {
            // This code was changed from doing string concatenations to using string.Join
            // It is much faster, add a line at a time to the StringBuilder object.
            // Then dump the StringBuilder.ToString() to a file.
            StringBuilder sb = new StringBuilder();

            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            for (int i = 0; i < data.GetLength(0); i++)
            {

                string[] theLineData = new string[data.GetLength(1)];
                for (int l = 0; l < data.GetLength(1); l++)
                {
                    theLineData[l] = data[i, l];
                }
                string theLine = string.Join(",", theLineData);
                sb.AppendLine(theLine);
            }
            File.WriteAllText(filename, sb.ToString());

            // OLD CODE - JUST A REMINDER WHILE WE'RE TESTING
            //FileStream fs = null;
            //StreamWriter sw = null;

            //            //check length of filename and throw really sensible error.
            //            if (filename.Length > 259) 
            //            {
            //                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            //            }

            //          try
            //{
            //    fs = new FileStream(filename, FileMode.CreateNew);
            //    sw = new StreamWriter(fs);
            //    for (int i = 0; i < data.GetLength(0); i++)
            //    {
            //        string line = String.Empty;
            //        for (int l = 0; l < data.GetLength(1) - 1; l++)
            //        {
            //            line = line + data[i, l] + ",";
            //        }
            //        line = line + data[i, data.GetLength(1) - 1];
            //        sw.WriteLine(line);

            //        // The analysis team experienced random errors with the final rows of a file not being fully populated.
            //        // Added the flush statement to try an eliminate this error.
            //        sw.Flush();
            //    }
            //}
            //finally
            //{
            //    if (sw != null)
            //    {
            //        sw.Close();
            //    }
            //}
        }

        /// <summary>
        /// This method will populate a comma seperated file from the specified 2d array of strings.
        /// </summary>
        /// <param name="data">The 2d array of strings to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(double[,] data, string filename)
        {
            // This code was changed from doing string concatenations to using string.Join
            // It is much faster, add a line at a time to the StringBuilder object.
            // Then dump the StringBuilder.ToString() to a file.
            StringBuilder sb = new StringBuilder();

            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            for (int i = 0; i < data.GetLength(0); i++)
            {

                string[] theLineData = new string[data.GetLength(1)];
                for (int l = 0; l < data.GetLength(1); l++)
                {
                    theLineData[l] = data[i, l].ToString();
                }
                string theLine = string.Join(",", theLineData);
                sb.AppendLine(theLine);
            }
            File.WriteAllText(filename, sb.ToString());



            //FileStream fs = null;
            //StreamWriter sw = null;


            //          //check length of filename and throw really sensible error.
            //            if (filename.Length > 259)
            //            {
            //                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            //            }

            //try
            //{
            //    fs = new FileStream(filename, FileMode.CreateNew);
            //    sw = new StreamWriter(fs);
            //    for (int i = 0; i < data.GetLength(0); i++)
            //    {
            //        string line = String.Empty;
            //        for (int l = 0; l < data.GetLength(1) - 1; l++)
            //        {
            //            line = line + data[i, l].ToString("F22") +",";
            //        }
            //        line = line + data[i, data.GetLength(1) - 1].ToString("F22");
            //        sw.WriteLine(line);

            //        // The analysis team experienced random errors with the final rows of a file not being fully populated.
            //        // Added the flush statement to try an eliminate this error.
            //        sw.Flush();
            //    }
            //}
            //finally
            //{
            //    if (sw != null)
            //    {
            //        sw.Close();
            //    }
            //}
        }

        /// <summary>
        /// This method will populate a comma seperated file from the specified 1d array of double.
        /// </summary>
        /// <param name="data">The 1d array of doubles to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(double[] data, string filename)
        {
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            string[] theLineData = new string[data.GetLength(0)];
            for (int i = 0; i < data.GetLength(0); i++)
            {
                theLineData[i] = data[i].ToString();
            }
            string theLine = string.Join(",", theLineData);
            File.WriteAllText(filename, theLine);


            //FileStream fs = null;
            //StreamWriter sw = null;


            //            //check length of filename and throw really sensible error.
            //            if (filename.Length > 259)
            //            {
            //                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            //            }
            //          try
            //{
            //    fs = new FileStream(filename, FileMode.CreateNew);
            //    sw = new StreamWriter(fs);
            //    for (int i = 0; i < data.GetLength(0); i++)
            //    {
            //        string line = String.Empty;
            //        line = line + data[i].ToString("F22");
            //        sw.WriteLine(line);

            //        // The analysis team experienced random errors with the final rows of a file not being fully populated.
            //        // Added the flush statement to try an eliminate this error.
            //        sw.Flush();
            //    }
            //}
            //finally
            //{
            //    if (sw != null)
            //    {
            //        sw.Close();
            //    }
            //}
        }

        /// <summary>
        /// This method will populate a comma seperated file from the specified 1d array of int.
        /// </summary>
        /// <param name="data">The 1d array of int to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(int[] data, string filename)
        {
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            string[] theLineData = new string[data.GetLength(0)];
            for (int i = 0; i < data.GetLength(0); i++)
            {
                theLineData[i] = data[i].ToString();
            }
            string theLine = string.Join(",", theLineData);
            File.WriteAllText(filename, theLine);

            //FileStream fs = null;
            //StreamWriter sw = null;

            //            //check length of filename and throw really sensible error.
            //            if (filename.Length > 259)
            //            {
            //                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            //            }

            //          try
            //{
            //    fs = new FileStream(filename, FileMode.CreateNew);
            //    sw = new StreamWriter(fs);
            //    for (int i = 0; i < data.GetLength(0); i++)
            //    {
            //        string line = String.Empty;
            //        line = line + data[i].ToString();
            //        sw.WriteLine(line);

            //        // The analysis team experienced random errors with the final rows of a file not being fully populated.
            //        // Added the flush statement to try an eliminate this error.
            //        sw.Flush();
            //    }
            //}
            //finally
            //{
            //    if (sw != null)
            //    {
            //        sw.Close();
            //    }
            //}
        }

        /// <summary>
        /// This method will populate a comma seperated file from the specified 1d array of string.
        /// </summary>
        /// <param name="data">The 1d array of string to write to the .csv file.</param>
        /// <param name="filename">The fully justified filename of the file to create and store.</param>
        public static void WriteCSVFile(string[] data, string filename)
        {
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            string theLine = string.Join(",", data);
            File.WriteAllText(filename, theLine);
        }

        /// <summary>
        /// This method will read a comma separated file into a 1d array of string.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 1d array of string populated from the comma separated file.</returns>
        public static string[] ReadCSVFileStringVector(string filename)
        {
            string[] theData = null;
            StreamReader sr = null;
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = sr.ReadLine();

                if (line != null)
                {
                    string trimmedLine = line.Trim();
                    theData = trimmedLine.Split(',');
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }

            return theData;
        }

        /// <summary>
        /// This method will read a comma separated file into a 2d array of double.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 2d array of double populated from the comma separated file.</returns>
        public static double[,] ReadCSVFile2DDoubleArray(string filename)
        {
            List<List<double>> theData = new List<List<double>>();
            StreamReader sr = null;
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = null;
                int numberColumns = -1;

                while ((line = sr.ReadLine()) != null)
                {

                    string[] items = line.Split(',');
                    if (numberColumns == -1)
                    {
                        numberColumns = items.Count();
                    }
                    if (items.Count() != numberColumns)
                    {
                        throw new Exception("the data file " + filename + " contains a variable number of columns within its data, this cannot be read");
                    }

                    List<double> theValues = new List<double>();
                    for (int l = 0; l < items.Count(); l++)
                    {
                        double valueToAdd = Convert.ToDouble(items[l]);
                        theValues.Add(valueToAdd);
                    }
                    theData.Add(theValues);
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }
            int rows = theData.Count;
            int columns = theData[0].Count;
            double[,] theRet = new double[rows, columns];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    theRet[r, c] = theData[r][c];
                }
            }

            return theRet;
        }

        /// <summary>
        /// This method will read a comma separated file into a 1d array of double.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 1d array of double populated from the comma separated file.</returns>
        public static double[] ReadCSVFileDoubleVector(string filename)
        {
            List<double> theData = new List<double>();
            StreamReader sr = null;
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = null;

                while ((line = sr.ReadLine()) != null)
                {
                    double valueToAdd = Convert.ToDouble(line.Trim());
                    theData.Add(valueToAdd);
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }

            return theData.ToArray();
        }

        /// <summary>
        /// This method will read a comma separated file into a 1d array of int.
        /// </summary>
        /// <param name="filename">The comma separated file, fully justified path.</param>
        /// <returns>The 1d array of int populated from the comma separated file.</returns>
        public static int[] ReadCSVFileIntVector(string filename)
        {
            List<int> theData = new List<int>();
            StreamReader sr = null;
            //check length of filename and throw really sensible error.
            if (filename.Length > 259)
            {
                throw new Exception("Filename Length Error -- the filename " + filename + " is " + filename.Length + " characters. Filenames must be less than 260 characters, Directory names must be less than 248 characters.");
            }

            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open);
                sr = new StreamReader(fs);
                string line = null;

                while ((line = sr.ReadLine()) != null)
                {
                    int valueToAdd = Convert.ToInt32(line.Trim());
                    theData.Add(valueToAdd);
                }
            }
            finally
            {
                // Closing the StreamReader also closes the FileStream.
                if (sr != null)
                {
                    sr.Close();
                }
            }

            return theData.ToArray();
        }
    }
}
