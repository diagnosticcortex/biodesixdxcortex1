﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

///////////////////////////////////////////////////////////////////////////////
//
//      Filename: OpenXmlExcelReader.cs
//
//      (C) Copyright 2009 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ed Rotthoff    July 23 2012   created this class to implement the read operations for
//                                    the excel interface using Open XML.
//      Ben Linstid    Oct 17 2012    added check for null CellValue in GetCellValue.
//      Ed Rotthoff    Oct 24 2102    make the read more robust when there are empty cells.
//      Ed Rotthoff    Oct 25 2012    make the GetCellValue method public for use in the OpenXmlExcelWriter class.
//      Conde Benoist  April 9 2013   line 432 cell number formatid changed to start at 1 instead of 0. 
//      Conde Benoist  April 25 2013  line 432 now throws an exception when trying to read a corrupted cell. 
//                                    the cell is not empty, but the reader considers it null. 
//      Ed Rotthoff    May 6 2013     add better support for date formats, and custom numbering formats.
//      Ed Rotthoff    May 06 2013     fix date format bug
//      Ed Rotthoff    May 07 2013     remove console.writeline debug code..
//      Conde Benoist  8.15.2013      Ln 148 Out of range exception when an empty worksheet is encountered.
//      Conde Benoist  8.21.2013      Added 2 public methods - one for renaming duplicate columns when returning a datatable from a worksheet
//                                    and another method for returns a bool if worksheet information contains more data columns than header columns.
//      Ed Rotthoff     8/22/2016    Add logic to check the length of the filename  (218 chars) and length of the worksheet name (31 chars).
//      Conde Benoist   10.5.2016       Added additional logic to the GetCellValue method to determine if a cell uses a custom date format, if so, return a standard date.
//      
///////////////////////////////////////////////////////////////////////////////
namespace Central.Util
{
    /// <summary>
    /// this class implements the methods used to access information contained within Excel.
    /// the class can be used to read information from an MS Excel 2007 or higher document. This class uses
    /// the OpenXML interface to excel, so there is no limitations to using this other than the
    /// excel file must be 2007 or a more recent version of Excel. the spreadsheet file to be used must be .xlsx.
    /// Limitations: this class does not yet handle password protected spreadsheets.
    /// </summary>
    public class OpenXmlExcelReader
    {
			/// <summary>
			/// THis is the max length allowed for an excel filename, the application issues errors if the filename is
			/// longer than this, and the errors are not quite user friendly.
			/// </summary>
			public static int MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME = 218;
			/// <summary>
			/// This method should be used to check that the excel filename is under the legal length for the excel application.
			/// the excel app presently reports errors that are not as clear as they might be for this error.
			/// </summary>
			/// <param name="fullyJustifiedExcelFilename">the fully justified filename for the excel file</param>
			public static void CheckLengthOFExcelFilename(string fullyJustifiedExcelFilename)
			{
				if (fullyJustifiedExcelFilename.Length > MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME)
				{
					throw new Exception(" OpenXMLExcelReader -- illegal filename, the excel application allows filenames not in excess of " + MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME + " characters, your filename " + fullyJustifiedExcelFilename + " is " + fullyJustifiedExcelFilename.Length + " characters, please relocate the file to another location, or shorten the filename to be less than " + MAX_LENGTH_EXCEL_FULLY_JUSTIFIED_FILENAME + " characters.");
				}
			}
			/// <summary>
			/// THis is the max length allowed for an excel worksheet, the application issues errors if the worksheet is
			/// longer than this, and the errors are not quite user friendly.
			/// </summary>
			public static int MAX_LENGTH_EXCEL_WORKSHEET_NAME = 31;
			/// <summary>
			/// This method should be used to check that the excel worksheet is under the legal length for the excel application.
			/// the excel app presently reports errors that are not as clear as they might be for this error.
			/// </summary>
			/// <param name="worksheetName">the name of the worksheet</param>
			public static void CheckLengthOFExcelWorksheet(string worksheetName)
			{
				if (worksheetName.Length > MAX_LENGTH_EXCEL_WORKSHEET_NAME)
				{
					throw new Exception(" OpenXMLExcelReader -- illegal worksheet name, the excel application allows worksheet names not in excess of " + MAX_LENGTH_EXCEL_WORKSHEET_NAME + " characters, your worksheet " + worksheetName + " is " + worksheetName.Length + " characters, please use a shorter worksheet name that is less than " + MAX_LENGTH_EXCEL_WORKSHEET_NAME + " characters.");
				}
			}

        /// <summary>
        /// this method will return the list of worksheet names that are contained within the specified spreadsheet file.
        /// </summary>
        /// <param name="fullyJustifiedExcelFilename">the fully justified MS Excel File version 2007 or later.</param>
        /// <returns>the list of worksheet names contained within the specified spreadsheet.</returns>
        public static string[] GetWorksheets(string fullyJustifiedExcelFilename)
        {

					CheckLengthOFExcelFilename(fullyJustifiedExcelFilename);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedExcelFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                List<string> theNames = new List<string>();
                foreach (Sheet s in sheets)
                {
                    theNames.Add(s.Name);
                }
                string[] retVal = theNames.ToArray();
                return retVal;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
            
        }
        /// <summary>
        /// this method will read the specified worksheet from the specified fully justifed spreadsheet filename.
        /// the resulting data will be placed into a datatable with all of the cells being string. the resulting data table
        /// may then be queried using DataTable.Select(sqlWhereClause), or using linq. please see the code examples included.
        /// </summary>
        /// <example>
        /// to get specific rows, just use a SQL where clause
        /// <code>
        ///  DataTable theDT = Central.Util.OpenXmlExcelReader.GetDataTable(this.txtBxSelectedSpreadsheet.Text, this.cmboBxSelectedWorksheet.Text);
        ///
        ///        DataRow[] theRows = null;
        ///        
        ///            theRows = theDT.Select("((Convert(columnName,'System.Decimal') %LT% 20) AND (NOT(columnName == NULL)))");
        ///
        ///        DataSet ds = Central.Util.OpenXmlExcelReader.MakeDataSetFromResults(theDT, theRows);
        ///        
        ///        //if specific columns are needed use linq on ds.Tables[0] as in the next code example.
        ///
        /// </code>
        /// to get a specific column within the resulting datatable, use linq with a select.
        /// <code>
        /// DataTable theDT = Central.Util.OpenXmlExcelReader.GetDataTable(this.txtBxSelectedSpreadsheet.Text, this.cmboBxSelectedWorksheet.Text);
        ///       
        ///        var result = (from row in theDT.AsEnumerable()
        ///                      where !(String.IsNullOrEmpty(row.Field%LT%string%GT%("columnName")))
        ///                      select (string)(row.Field%LT%string%GT%("columnName")));
        ///
        ///        if (result != null)
        ///        {
        ///            List %LT%string%GT% theResults = new List%LT%string%GT%(result);
        ///            if (theResults.Count == 0)
        ///            {
        ///                throw new Exception("Error Reading the unique values. no unique values found.");
        ///            }
        ///
        ///            this.lstbxSelectedCriteria.Items.Clear();
        ///
        ///            foreach (string s in theResults)
        ///            {
        ///                string theVal = s;
        ///                if ((theVal != null) && (theVal.Length %GT% 0))
        ///                {
        ///                    if (!(lstbxSelectedCriteria.Items.Contains(theVal)))
        ///                    {
        ///                        lstbxSelectedCriteria.Items.Add(theVal);
        ///                    }
        ///                }
        ///            }
        ///            
        /// </code>
        /// 
        /// </example>
        /// <param name="fullyJustifiedSpreadsheetFilename">the fully justified filename of the spreadsheet to be used. must be 2007 xlsx or higher</param>
        /// <param name="worksheetName">the worksheet name to be read from the specified spreadsheet.</param>
        /// <returns>a datatable containing columns named according to the column headers in the WS, and populated with the ws data
        /// all cell values are text.</returns>
        public static DataTable GetDataTable(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
					CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    throw new Exception(" Error -- Worksheet named " + worksheetName + " Not found in spreadsheet " + fullyJustifiedSpreadsheetFilename);
                }
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)theDoc.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                
                DataTable retDT = new DataTable();

                if (rows.Count<Row>() == 0)
                {
                    throw new Exception("OpenXMLReader: Error -- " + worksheetName + " appears to be empty.");
                }
                int numColHeaders = rows.ElementAt(0).Descendants<Cell>().Count();
                List<string> theHeaderColumnNames = new List<string>();

                foreach (Cell cell in rows.ElementAt(0)) 
                {
                    retDT.Columns.Add(GetCellValue(theDoc, cell)); 
                    string excelColumnName = GetExcelColumnName(cell.CellReference.InnerText.ToString());
                    theHeaderColumnNames.Add(excelColumnName);
                }
                int cnt = 0;
                foreach (Row row in rows)       
                {
                    if (cnt > 0) //skip the header row.
                    {
                        if (row.Descendants<Cell>() != null)
                        {
                            //get the non empty cells, if all empty this is ok.
                            IEnumerable<Cell> cellValues = (from cell in row.Descendants<Cell>()
                                                                  where cell.CellValue != null
                                                                   select cell);
                            //skip it if its all empty.
                            if(cellValues.Count<Cell>() > 0)
                            {
                                DataRow tempRow = retDT.NewRow();
                                //if a row has empty cells these will be left out of the collection.
                                //so we have to introspect the cell reference to get the proper column.
                                for (int i = 0; i < row.Descendants<Cell>().Count<Cell>(); i++)
                                {
                                    Cell c = row.Descendants<Cell>().ElementAt(i);
                                    string cellColumn = GetExcelColumnName(c.CellReference.InnerText.ToString());
                                    int ci = theHeaderColumnNames.IndexOf(cellColumn);
                                    if (ci >= 0 && ci < theHeaderColumnNames.Count)
                                    {
                                        tempRow[ci] = GetCellValue(theDoc, c);
                                    }
                                }
                                retDT.Rows.Add(tempRow);
                            }
                        }
                    }
                    cnt++;
                } 
                return retDT;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }


        /// <summary>
        /// returns a datatable when supplied a fully justified spreadsheet filename and worksheet name.
        /// if the original worksheet contains duplicate column names (not case sensitive), the first instance of the column name will be retained.
        /// each consectutive duplicate name will get an underscore and an integer value appended to it's name.
        /// 
        /// e.g. column name = 'MyColumn'
        /// 1st instance - 'MyColumn'
        /// 2nd instance - 'MyColumn_1'
        /// 3rd instance - 'mycolumn_2' (ignores case)
        /// 
        /// up until 256. At this point an exception would be thrown if two column names exist with the same name..
        /// 'MyColumn_256'
        /// this is just an arbitrary stop point which can be modified in the 'ReturnUniqueColumnName()' private method.
        /// </summary>
        /// <param name="fullyJustifiedSpreadsheetFilename"></param>
        /// <param name="worksheetName"></param>
        /// <returns></returns>
        public static DataTable GetDataTableWithUniqueColumnNames(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
					CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    throw new Exception(" Error -- Worksheet named " + worksheetName + " Not found in spreadsheet " + fullyJustifiedSpreadsheetFilename);
                }
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)theDoc.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                DataTable retDT = new DataTable();

                if (rows.Count<Row>() == 0)
                {
                    throw new Exception("OpenXMLReader: Error -- " + worksheetName + " appears to be empty.");
                }
                int numColHeaders = rows.ElementAt(0).Descendants<Cell>().Count();

                List<string> theHeaderColumnNames = new List<string>();
                List<string> theHeaderColumnReferences = new List<string>();  
              

                foreach (Cell cell in rows.ElementAt(0))
                {                
                    // since it is the first row.. this will be the column header/name
                    string originalColumnName = GetCellValue(theDoc, cell);
                    string uniqueColumnName = ReturnUniqueColumnName(originalColumnName, theHeaderColumnNames);   
                    retDT.Columns.Add(uniqueColumnName);
                    theHeaderColumnNames.Add(uniqueColumnName);

                    // Excel Column Reference- i.e. 'A', 'B', etc.
                    string excelColumnReference = GetExcelColumnName(cell.CellReference.InnerText.ToString());
                    theHeaderColumnReferences.Add(excelColumnReference);
                }
                int cnt = 0;
                foreach (Row row in rows)
                {
                    if (cnt > 0) //skip the header row.
                    {
                        if (row.Descendants<Cell>() != null)
                        {
                            //get the non empty cells, if all empty this is ok.
                            IEnumerable<Cell> cellValues = (from cell in row.Descendants<Cell>()
                                                            where cell.CellValue != null
                                                            select cell);
                            //skip it if its all empty.
                            if (cellValues.Count<Cell>() > 0)
                            {
                                DataRow tempRow = retDT.NewRow();
                                //if a row has empty cells these will be left out of the collection.
                                //so we have to introspect the cell reference to get the proper column.
                                for (int i = 0; i < row.Descendants<Cell>().Count<Cell>(); i++)
                                {
                                    Cell c = row.Descendants<Cell>().ElementAt(i);
                                    string cellColumn = GetExcelColumnName(c.CellReference.InnerText.ToString());
                                    int ci = theHeaderColumnReferences.IndexOf(cellColumn);
                                    if (ci >= 0 && ci < theHeaderColumnReferences.Count)
                                    {
                                        tempRow[ci] = GetCellValue(theDoc, c);
                                    }
                                }
                                retDT.Rows.Add(tempRow);
                            }
                        }
                    }
                    cnt++;
                }
                return retDT;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }


        /// <summary>
        /// this method will return the list of column header values contained within the specified worksheet within the
        /// specified spreadsheet
        /// </summary>
        /// <param name="fullyJustifiedSpreadsheetFilename">the spreadsheet to look within, must be excel 2007 or higher</param>
        /// <param name="worksheetName">the worksheeet name to look within</param>
        /// <returns>the list of header columns in the specified worksheet in the specified spreadsheet.</returns>
        public static string[] GetColumnNames(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
					CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    throw new Exception(" Error -- Worksheet named " + worksheetName + " Not found in spreadsheet " + fullyJustifiedSpreadsheetFilename);
                }
                string relationshipId = sheets.First().Id.Value; 
                WorksheetPart worksheetPart = (WorksheetPart)theDoc.WorkbookPart.GetPartById(relationshipId); 
                Worksheet workSheet = worksheetPart.Worksheet; 
                SheetData sheetData = workSheet.GetFirstChild<SheetData>(); 
                IEnumerable<Row> rows = sheetData.Descendants<Row>(); 

                List<string> theNames = new List<string>();
                foreach (Cell cell in rows.ElementAt(0)) 
                {
                    string theValue = GetCellValue(theDoc, cell);
                    if (theValue.Length > 0)
                    {
                        theNames.Add(theValue);
                    }
                } 
                string[] retVal = theNames.ToArray();
                return retVal;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }

        /// <summary>
        /// this method will return true if the specified worksheet is present within the specified spreadsheet
        /// it will return false if the specified worksheet is not present in the specified spreadsheet
        /// </summary>
        /// <param name="fullyJustifiedSpreadsheetFilename">the excel spreadsheet, must be excel 2007 or higher</param>
        /// <param name="worksheetName">the worksheet name</param>
        /// <returns>true if the worksheet is present in the spreadsheet, false if the worksheet is not present in the spreadsheet</returns>
        public static bool WorksheetExists(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
					CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    return false;

                }
                return true;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }
        }

        /// <summary>
        /// this method will make a dataset using the columns in the source data table
        /// and the rows in the results data set. use this to make data sets after calling
        /// DataTable.Select. This places the resultDataRows into the result dataset.Tables[0]
        /// </summary>
        /// <param name="sourceDT">the source data table which select was invoked upon. </param>
        /// <param name="resultsDataRows">the rows resulting from the select statement</param>
        /// <returns>a DataSet populated with the results data rows, these are in the result DataSet.Tables[0]</returns>
        public static DataSet MakeDataSetFromResults(DataTable sourceDT, DataRow[] resultsDataRows)
        {
            DataSet ds = new DataSet();
            DataTable theDT1 = new DataTable();
            foreach (DataColumn dc in sourceDT.Columns)
            {
                theDT1.Columns.Add(new DataColumn(dc.ColumnName));
            }
            foreach (DataRow dr in resultsDataRows)
            {
                DataRow newdr = theDT1.NewRow();
                foreach (DataColumn dc in theDT1.Columns)
                {
                    newdr[dc.ColumnName] = dr[dc.ColumnName];
                }
                theDT1.Rows.Add(newdr);
            }

            ds.Tables.Add(theDT1);

            return ds;
        }

        /// <summary>
        /// this method will make a data set containing the list of data rows specified, and the list of specified columns.
        /// </summary>
        /// <param name="sourceDT">the source datatable</param>
        /// <param name="theColsToUse">the columns to use from the source datatable</param>
        /// <param name="resultsDataRows">the rows to add to the results datatable.</param>
        /// <returns>a dataset containing a datatable which holds the specified rows and columns.</returns>
        public static DataSet MakeDataSetFromResults(DataTable sourceDT, List<string>theColsToUse, DataRow[] resultsDataRows)
        {
            DataSet ds = new DataSet();
            DataTable theDT1 = new DataTable();
            foreach (string s in theColsToUse)
            {
                theDT1.Columns.Add(new DataColumn(s));
            }
            foreach (DataRow dr in resultsDataRows)
            {
                DataRow newdr = theDT1.NewRow();
                foreach (string s in theColsToUse)
                {
                    newdr[s] = dr[s];
                }
                theDT1.Rows.Add(newdr);
            }

            ds.Tables.Add(theDT1);

            return ds;
        }

        /// <summary>
        /// this method will get the excel column name, for example A,B,C,D etc. for the specified excel cellname.
        /// </summary>
        /// <param name="cellName">the cell name</param>
        /// <returns>the MS excel column name of the cell</returns>
        private static string GetExcelColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[A-Za-z]+");
            System.Text.RegularExpressions.Match match = regex.Match(cellName);

            return match.Value;
        }



        /// <summary>
        /// this is the method that will read the contents of a call and return the string value of the contents.
        /// </summary>
        /// <param name="document">the spreadsheet document that is being read.</param>
        /// <param name="cell">the cell within the spreadsheet document.</param>
        /// <returns></returns>
        public static string GetCellValue(SpreadsheetDocument document, Cell cell) 
        { 
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            List<CellFormats> cellFormats = new List<CellFormats>(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Descendants<CellFormats>());
            List<CellFormat> useCellFormats = cellFormats[0].Descendants<CellFormat>().ToList();
            List<NumberingFormats> numberingFormats = new List<NumberingFormats>(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Descendants<NumberingFormats>());
           // this code commented out because we will not use the numbering formats.
            // if we do decide to use these in the future, then understand sometimes
            //the numberingFormats is null, meaning there are none used.
            //List<NumberingFormat> useNumberingFormats = numberingFormats[0].Descendants<NumberingFormat>().ToList();
            /*
             * 
             *    numberingFormatID definitions. these numbering formats do not exist in the excel document, 
             *    and are standard for the excel application so we must just 'know' them!!!
             *    IF the numberingFormatID is outside of this range, then go and look in the numbering formats specified in the document.
             *    Find the one with the matching id, and use it.
             * 
             *   --------numberingFormatID----------Format------------------------------------
                            1                        0
                            2                        0.00
                            3                        #,##0
                            4                        #,##0.00
                            5                        $#,##0_);($#,##0)
                            6                        $#,##0_);[Red]($#,##0)
                            7                        $#,##0.00_);($#,##0.00)
                            8                        $#,##0.00_);[Red]($#,##0.00)
                            9                        0%
                            10                       0.00%
                            11                       0.00E+00
                            12                       # ?/?
                            13                       # ??/??
                            14                       m/d/yyyy
                            15                       d-mmm-yy
                            16                       d-mmm
                            17                       mmm-yy
                            18                       h:mm AM/PM
                            19                       h:mm:ss AM/PM
                            20                       h:mm
                            21                       h:mm:ss
                            22                       m/d/yyyy h:mm
                            37                       #,##0_);(#,##0)
                            38                       #,##0_);[Red](#,##0)
                            39                       #,##0.00_);(#,##0.00)
                            40                       #,##0.00_);[Red](#,##0.00)
                            45                       mm:ss
                            46                       [h]:mm:ss
                            47                       mm:ss.0
                            48                       ##0.0E+0
                            49                       @
             */

            //we will check for strongly formatted cells, but will just do a general conversion for these cells.....
            //we could write alot of code to ensure the data gets into the same format as the user sees within the 
            //excel document, but since we will ask within biodesix that all cells be formatted as text, this would be overkill.
            //we support strongly formatted cells for the cases where we receive SS that do not adhere to Biodesix internal
            //conventions, so we can still use these.

            //So excel does the following,
            //a cell can have a data type, IF a cell does NOT have a data type
            //specified then the cell formatting specifies what is in the cell...
            //
            //each cell format, refers to a numbering format, and numbering formats have a
            //set of "standard ids" that we should just "know" and are NOT defined
            //within the Excel document anywhere.
            //if the numbering format id is NOT in the set of standard ids then the numbering
            //format will be defined within the Excel document xml.
            //numbering formats may be introspected to gain knowledge of the Excel specific
            //regEx that is used for formatting the cell, this needs to be translated to a c#
            //regEx in order to maintain cell formats.

            //if the cell type is not set, then use the numbering formats.
            if (cell.DataType == null)
            {
                if (cell.StyleIndex == null)
                {
                    if (cell.CellValue == null)
                    {
                        throw new Exception("OpenXmlExcelReader -- Error: The cell at address "
                            + cell.CellReference.ToString()
                            + " seems to be corrupt! Please try copying all of the populated cells from the current worksheet"
                            + " to a new worksheet and try again. If the problem persists, contact a Biodesix software developer.");
                    }
                    return cell.CellValue.Text;
                }
                //get the cell format associated with this cell.
                CellFormat cf = useCellFormats[Convert.ToInt32(cell.StyleIndex.Value)];
                //do a primitive check for number, date, or just text, and output a generically formatted string.
                if (cf.NumberFormatId > 0 && cf.NumberFormatId <= 13)
                {// This is a number   
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        //double here to support all formats including scientific notation!
                        return Double.Parse(cell.CellValue.Text).ToString("##0.0##");
                    }     
                }
                else if (cf.NumberFormatId == 14)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy");
                    }
                }
                else if (cf.NumberFormatId == 15)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("d-MMM-yy");
                    }
                }
                else if (cf.NumberFormatId == 16)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("d-MMM");
                    }
                }
                else if (cf.NumberFormatId == 17)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("MMM-yy");
                    }
                }
                else if (cf.NumberFormatId == 18)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm AM/PM");
                    }
                }
                else if (cf.NumberFormatId == 19)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm:ss AM/PM");
                    }
                }
                else if (cf.NumberFormatId == 20)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm");
                    }
                }
                else if (cf.NumberFormatId == 21)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("h:mm:ss");
                    }
                }
                else if (cf.NumberFormatId == 22)
                {// This is a date, but in Excel its just a number relative to the 'start of time' which excel uses.  
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy h:mm");
                    }
                }
                else if (cf.NumberFormatId > 163) //greater than 163 is a custom numbering format.
                {
                    if (cell.CellValue == null)
                    {
                        return cell.InnerText.ToString();
                    }
                    else
                    {
                        string format = document.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats.Elements<NumberingFormat>()
                                          .Where(i => i.NumberFormatId.ToString() == cf.NumberFormatId.ToString())
                                            .First().FormatCode;

                        // Determine if a custom number format was used for a date value. Custom formatting in Excel may take many forms
                        // such as '[$-409]ddmmmyyyy' which includes a locale ([$-409] for US dates) or 'yy MMM'. if it can be
                        // determined that the cell is some kind of custom date format, format the cell value as a standard Biodesix date.
                        if ((!string.IsNullOrEmpty(format)) && ((format.StartsWith("[$-")) || (format.ToLower().Contains("yy"))))
                        {
                            return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString("M/d/yyyy");
                        }
                        else if (!String.IsNullOrEmpty(format))
                        {
                            // some other unknown format is used, use the format found from the cell 
                            double d = Convert.ToDouble(cell.CellValue.Text);
                            return d.ToString(format);
                        }
                        else
                        {
                            return cell.CellValue.Text;
                        }

                    }
                }
                else if (cell.CellValue == null)
                {//just text
                    return cell.InnerText.ToString();
                }
                else
                {
                    return cell.CellValue.Text;
                }
            }

            //if the data type is set, use this.
            switch (cell.DataType.Value)
            {
                case CellValues.SharedString:
                    {//if its a chared string, then get the value of the cell from the shared string table.
                        return stringTablePart.SharedStringTable.ChildElements[Convert.ToInt32(cell.CellValue.Text)].InnerText;
                    }
                case CellValues.Boolean:
                    {
                        //if its a boolean then convert it to the value 'true' or 'false'
                        if (cell.CellValue.Text == "1")
                        {
                            return "true";
                        }
                        else
                        {
                            return "false";
                        }
                    }
                case CellValues.Date:
                    { //if its a date then it is a number that specifies the offset from the start of time, convert this to a date
                        // and output the date string.
                        return DateTime.FromOADate(Convert.ToDouble(cell.CellValue.Text)).ToString();
                    }
                case CellValues.Number:
                    {
                        //if its a number just use the cellvalue.
                        return cell.CellValue.Text;
                    }
                default:
                    {//if its none of these then just use the cellvalue if it is present.
                        if (cell.CellValue != null)
                        {
                            return cell.CellValue.Text;
                        }
                    }
                    // return an empty value.
                    return string.Empty;
            }
        }

        /// <summary>
        /// used to determine if the current column name already exists. 
        /// if the column name already exists (ignores case), a unique, integer appended name will be returned.
        /// 'MyColumnName_1' if 'MyColumnName' already exists. will append an integer value up until 256.
        /// </summary>
        /// <param name="originalColumnName"></param>
        /// <param name="currentColumnNameList"></param>
        /// <returns></returns>
        private static string ReturnUniqueColumnName(string originalColumnName, List<string> currentColumnNameList)
        {
            string uniqueColumnName = originalColumnName;
            
            for (int i = 1; i < 257; i++)
            {
                bool originalNameIsUnique = true;
                foreach (string existingColumnName in currentColumnNameList)
                {
                    if (existingColumnName.ToUpper().Equals(uniqueColumnName.ToUpper()))
                    {
                        originalNameIsUnique = false;
                        break;
                    }
                }

                // original column name is unique, break out of the 'for' loop and exit the method.
                if (originalNameIsUnique)
                {
                    break;
                }
                // name is not unique (already exists in the datatable) append an integer and compare again against the original list of column names
                else    
                {                    
                    uniqueColumnName = originalColumnName + "_" + i.ToString();
                }
            }
            return uniqueColumnName;
        }

        /// <summary>
        /// returns true if the number of columns containing data exceeds the number of columns
        /// containing column headers.
        /// </summary>
        /// <param name="fullyJustifiedSpreadsheetFilename"></param>
        /// <param name="worksheetName"></param>
        /// <returns></returns>
        public static bool WorksheetDataIsMissingColumnHeader(string fullyJustifiedSpreadsheetFilename, string worksheetName)
        {
					CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					CheckLengthOFExcelWorksheet(worksheetName);

            SpreadsheetDocument theDoc = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, false);
            try
            {
                IEnumerable<Sheet> sheets = theDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetName) == 0);
                if ((sheets == null) || (sheets.Count() == 0))
                {
                    throw new Exception(" Error -- Worksheet named " + worksheetName + " Not found in spreadsheet " + fullyJustifiedSpreadsheetFilename);
                }
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)theDoc.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                if (rows.Count<Row>() == 0)
                {
                    throw new Exception("OpenXMLReader: Error -- " + worksheetName + " appears to be empty.");
                }
                int numColHeaders = rows.ElementAt(0).Descendants<Cell>().Count();

                List<string> theHeaderColumnReferences = new List<string>();
                List<string> theDataColumnReferences = new List<string>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    // Excel Column Reference- e.g., 'A', 'B', etc.
                    string excelColumnReference = GetExcelColumnName(cell.CellReference.InnerText.ToString());
                    theHeaderColumnReferences.Add(excelColumnReference);
                }

                int cnt = 0;
                foreach (Row row in rows)
                {
                    if (cnt > 0) //skip the header row.
                    {
                        foreach (Cell cell in rows.ElementAt(cnt))
                        {
                            // Excel Column Reference- e.g., 'A', 'B', etc.
                            string excelColumnReference = GetExcelColumnName(cell.CellReference.InnerText.ToString());
                            if (!theDataColumnReferences.Contains(excelColumnReference))
                            {
                                theDataColumnReferences.Add(excelColumnReference);
                            }
                        }
                    }
                    cnt++;
                }

                bool missingColumnNamesExist = false;

                // if data columns exceed header columns, there are columns with data that do not have a header.
                if (theDataColumnReferences.Count > theHeaderColumnReferences.Count)
                {
                    missingColumnNamesExist = true;
                }                
                else
                {
                    // verify that each DATA column has a matching HEADER column.
                    // (not concerned with Header columns that might not contain any Data)
                    foreach (string dataColumnReference in theDataColumnReferences)
                    {
                        missingColumnNamesExist = true;
                        foreach (string headerColumnReference in theHeaderColumnReferences)
                        {
                            if (dataColumnReference.Equals(headerColumnReference))
                            {
                                // when a matching HEADER column is found for the current DATA column, 
                                // clear the bool and go to the next DATA column.
                                missingColumnNamesExist = false;
                                break;
                            }
                        }
                        // if any data column does not have a header, no more evaluation is necessary.
                        if (missingColumnNamesExist)
                        {
                            break;
                        }
                    }
                }
                return missingColumnNamesExist;
            }
            finally
            {
                if (theDoc != null)
                {
                    theDoc.Close();
                }
            }

        }
    }
}
