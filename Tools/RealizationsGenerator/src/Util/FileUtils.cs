﻿///////////////////////////////////////////////////////////////////////////////
//
//      Filename: FileUtils.cs
//
//      (C) Copyright 2009 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ben Linstid     10/15/2010      Added interface for save file dialog, created this header.
//      Ed Rotthoff     Feb 1 2011      Added the GetBytes method, to support copying of streams.
//      Ben Linstid     Dec 1 2011      Added the ability to provide a suggested name to save dialog.
//      Ben Linstid     Mar 21 2012     Added a BrowseForDirectory method
//      Ben Linstid     June 06 2012    Added BrowseForFileToSaveNoOverwritePrompt method
//      Ed Rotthoff     Dec 5 2012      add the ability to set the title on the open file search dialog.
//      Ed Rotthoff     Nov 22 2013     add ability to show new folder button on directory browser.
//      Ed Rotthoff     11/17/2014      add new method to select a list of files from a directory, BrowseForFiles
//      Ben Linstid     06/29/2016      Added new methods that do Directory or DirectoryInfo GetFiles that remove duplicate file entries.
//      Conde Benoist   11.2.2018       Added a check if the directory exists for the BrowseForFile method.
//
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

namespace Central.Util
{
    /// <summary>
    /// This class contains methods that are used to facilitate the use of files, and streams throughout any application.
    /// </summary>
    public static class FileUtils
    {
        /// <summary>
        /// A directory used herein must contain at least 2 characters, a dirve letter and colon.
        /// </summary>
        public static int MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING = 2;
        /// <summary>
        /// This is the default directory location to be used throughout this class.
        /// </summary>
        public static string DEFAULT_DIRECTORY_LOCATION = "C:\\";
        /// <summary>
        /// This method allows the user to browse for files, the specified file filter
        /// is used to present an open file dialog. the selected fully justified filename 
        /// is returned, or "" if the user cancels the search process.
        /// </summary>
        /// <param name="regEx"> the filter to use in searching for files example
        ///     Xml Files(*.xml)|*.xml</param>
        /// <param name="tmpFileKeyForDirectoryName"> this is the key to use in the temp file utils to look up last used values</param>
        /// <returns>the fully justified filename, or "" if cancelled</returns>
        public static string BrowseForFile(string regEx, string tmpFileKeyForDirectoryName)
        {
           return  BrowseForFile(regEx, tmpFileKeyForDirectoryName, "Open");
        }
        /// <summary>
        /// This method lets the user browse for a file to open, it uses the specified tmp file key and regEx,
        /// and also sets the title of the dialog to the specified title
        /// </summary>
        /// <param name="regEx">the regex to use</param>
        /// <param name="tmpFileKeyForDirectoryName">the tmp file key to use.</param>
        /// <param name="titleForSearchDialog">the title for the search dialog.</param>
        /// <returns>null if the user cancels, the filename selected if the user selects.</returns>
        public static string BrowseForFile(string regEx, string tmpFileKeyForDirectoryName, string titleForSearchDialog)
        {
            string retVal = String.Empty;

            //open file dialog to use.
            System.Windows.Forms.OpenFileDialog theDialog = new System.Windows.Forms.OpenFileDialog();
            theDialog.Title = titleForSearchDialog;
            Central.Util.TempFileUtils tmpFile = new TempFileUtils();
            //this holds the last selected file
            string tmp = tmpFile.GetString(tmpFileKeyForDirectoryName);
            //if no file default to c:
            if ((tmp == null) || tmp.Length < MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING)
            {
                if (!Directory.Exists(tmp))
                {
                    tmp = DEFAULT_DIRECTORY_LOCATION;
                }
            }
            //set up the open file dialog
            theDialog.AddExtension = true;
            theDialog.Filter = regEx;
            theDialog.InitialDirectory = tmp;
            theDialog.CheckFileExists = false;
            theDialog.Multiselect = false;
            //check the dialog result after display
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //put the name into a file info 
                FileInfo finfo = new FileInfo(theDialog.FileName);
                //set the last chosen file.
                tmpFile.SetString(tmpFileKeyForDirectoryName, finfo.Directory.FullName);
                retVal = theDialog.FileName;
            }
            return retVal;
        }

        /// <summary>
        /// This method lets the user browse for multiple files to open, it uses the specified tmp file key and regEx,
        /// and also sets the title of the dialog to the specified title
        /// </summary>
        /// <param name="regEx">the regex to use</param>
        /// <param name="tmpFileKeyForDirectoryName">the tmp file key to use.</param>
        /// <param name="titleForSearchDialog">the title for the search dialog.</param>
        /// <returns>an empty list if the user cancels, the list of filenames selected if the user selects.</returns>
        public static List<string> BrowseForFiles(string regEx, string tmpFileKeyForDirectoryName, string titleForSearchDialog)
        {
            List<string> retVal = new List<string>();

            //open file dialog to use.
            System.Windows.Forms.OpenFileDialog theDialog = new System.Windows.Forms.OpenFileDialog();
            theDialog.Title = titleForSearchDialog;
            Central.Util.TempFileUtils tmpFile = new TempFileUtils();
            //this holds the last selected file
            string tmp = tmpFile.GetString(tmpFileKeyForDirectoryName);
            //if the filename is null, or if it is under the minimum length we require for a directory name
            //then display the default
            if ((tmp == null) || tmp.Length < MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING)
            {
                if (!Directory.Exists(tmp))
                {
                    tmp = DEFAULT_DIRECTORY_LOCATION;
                }
            }
            //set up the open file dialog
            theDialog.AddExtension = true;
            theDialog.Filter = regEx;
            theDialog.InitialDirectory = tmp;
            theDialog.CheckFileExists = false;
            theDialog.Multiselect = true;
            //check the dialog result after display
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //put the name into a file info 
                FileInfo finfo = new FileInfo(theDialog.FileName);
                //set the last chosen file.
                tmpFile.SetString(tmpFileKeyForDirectoryName, finfo.Directory.FullName);
                retVal = new List<string>(theDialog.FileNames);
            }
            return retVal;
        }

        /// <summary>
        /// This method allows the user to browse for files, the specified file filter
        /// is used to present a save file dialog. the selected fully justified filename 
        /// is returned, or "" if the user cancels the search process.
        /// </summary>
        /// <param name="regEx"> the filter to use in searching for files example
        ///     Xml Files(*.xml)|*.xml</param>
        ///     <param name="tmpFileKeyForDirectoryName">this is the temp file key to use for last used directory values</param>
        /// <returns>the fully justified filename, or "" if cancelled</returns>
        public static string BrowseForFileToSave(string regEx, string tmpFileKeyForDirectoryName)
        {
            string retVal = String.Empty;

            //open file dialog to use.
            System.Windows.Forms.SaveFileDialog theDialog = new System.Windows.Forms.SaveFileDialog();
            Central.Util.TempFileUtils tmpFile = new TempFileUtils();
            //this holds the last selected file
            string tmp = tmpFile.GetString(tmpFileKeyForDirectoryName);
            //if the filename is null, or if it is under the minimum length we require for a directory name
            //then display the default
            if ((tmp == null) || tmp.Length < MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING)
            {
                tmp = DEFAULT_DIRECTORY_LOCATION;
            }
            //set up the open file dialog
            theDialog.AddExtension = true;
            theDialog.Filter = regEx;
            theDialog.InitialDirectory = tmp;
            theDialog.CheckFileExists = false;
            //check the dialog result after display
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //put the name into a file info 
                FileInfo finfo = new FileInfo(theDialog.FileName);
                //set the last chosen file.
                tmpFile.SetString(tmpFileKeyForDirectoryName, finfo.Directory.FullName);
                retVal =  theDialog.FileName;
            }
            return retVal;
        }

        /// <summary>
        /// This method allows the user to browse for files, the specified file filter
        /// is used to present a save file dialog. the selected fully justified filename 
        /// is returned, or "" if the user cancels the search process.  The initial displayed filename
        /// is the suggested filename that is passed in to this method.
        /// </summary>
        /// <param name="regEx"> the filter to use in searching for files example
        ///     Xml Files(*.xml)|*.xml</param>
        ///     <param name="tmpFileKeyForDirectoryName">this is the temp file key to use for the last used directory</param>
        ///     <param name="suggestedFilename"> this is the value to use as the suggested filename</param>
        /// <returns>the fully justified filename, or "" if cancelled</returns>
        public static string BrowseForFileToSave(string regEx, string tmpFileKeyForDirectoryName, string suggestedFilename)
        {
            string retVal = String.Empty;

            //open file dialog to use.
            System.Windows.Forms.SaveFileDialog theDialog = new System.Windows.Forms.SaveFileDialog();
            Central.Util.TempFileUtils tmpFile = new TempFileUtils();
            //this holds the last selected file
            string tmp = tmpFile.GetString(tmpFileKeyForDirectoryName);
            //if the filename is null, or if it is under the minimum length we require for a directory name
            //then display the default
            if ((tmp == null) || tmp.Length < MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING)
            {
                tmp = DEFAULT_DIRECTORY_LOCATION;
            }
            //set up the open file dialog
            theDialog.AddExtension = true;
            theDialog.Filter = regEx;
            theDialog.InitialDirectory = tmp;
            theDialog.CheckFileExists = false;
            theDialog.FileName = suggestedFilename;
            //check the dialog result after display
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //put the name into a file info 
                FileInfo finfo = new FileInfo(theDialog.FileName);
                //set the last chosen file.
                tmpFile.SetString(tmpFileKeyForDirectoryName, finfo.Directory.FullName);
                retVal =  theDialog.FileName;
            }
            return retVal;
        }

        /// <summary>
        /// This method allows the user to browse for files, the specified file filter
        /// is used to present a save file dialog. the selected fully justified filename 
        /// is returned, or "" if the user cancels the search process.  The initial displayed filename
        /// is the suggested filename that is passed in to this method. No overwrite prompt
        /// will be displayed to the user if the user selects an existing file.
        /// </summary>
        /// <param name="regEx"> the filter to use in searching for files example
        ///     Xml Files(*.xml)|*.xml</param>
        ///     <param name="tmpFileKeyForDirectoryName">this is the temp file key to use for the last selected directory</param>
        ///     <param name="suggestedFilename"> this is the value to use for the dialog suggested filename to save.</param>
        /// <returns>the fully justified filename, or "" if cancelled</returns>
        public static string BrowseForFileToSaveNoOverwritePrompt(string regEx, string tmpFileKeyForDirectoryName, string suggestedFilename)
        {
            string retVal = String.Empty;
            //open file dialog to use.
            System.Windows.Forms.SaveFileDialog theDialog = new System.Windows.Forms.SaveFileDialog();
            Central.Util.TempFileUtils tmpFile = new TempFileUtils();
            //this holds the last selected file
            string tmp = tmpFile.GetString(tmpFileKeyForDirectoryName);
            //if the filename is null, or if it is under the minimum length we require for a directory name
            //then display the default
            if ((tmp == null) || tmp.Length < MIN_ACCEPTABLE_LENGTH_OF_DIRECTORY_STRING)
            {
                tmp = DEFAULT_DIRECTORY_LOCATION;
            }
            //set up the open file dialog
            theDialog.AddExtension = true;
            theDialog.Filter = regEx;
            theDialog.InitialDirectory = tmp;
            theDialog.CheckFileExists = false;
            theDialog.FileName = suggestedFilename;
            theDialog.OverwritePrompt = false;
            //check the dialog result after display
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //put the name into a file info 
                FileInfo finfo = new FileInfo(theDialog.FileName);
                //set the last chosen file.
                tmpFile.SetString(tmpFileKeyForDirectoryName, finfo.Directory.FullName);
                retVal =  theDialog.FileName;
            }
            return retVal;
        }

        /// <summary>
        /// Allows the user to browse for a directory.  The browsing starts at the 
        /// provided initial directory (provided it exists)and the dialog has the 
        /// provided dialog description.
        /// </summary>
        /// <param name="initialDirectory">the directory to set as the initial directory.</param>
        /// <param name="dialogDescription">the description to be placed in the top of the dialog</param>
        /// <returns>and empty string if the user cancels, the selected directory if the user selects to use the directory</returns>
        public static string BrowseForDirectory(string initialDirectory, string dialogDescription)
        {
            return BrowseForDirectory(initialDirectory,dialogDescription,false);
        }

        /// <summary>
        /// This method will display the folder browser and will display the specified dialog descriptioin, 
        /// the user may specify to show the create new folder button.
        /// </summary>
        /// <param name="initialDirectory">the startup directory for browsing</param>
        /// <param name="dialogDescription">the description on the dialog</param>
        /// <param name="showNewFolderButton">if true, allow the user to create new folders.</param>
        /// <returns>the fully justified folder selected by the user.</returns>
        public static string BrowseForDirectory(string initialDirectory, string dialogDescription, bool showNewFolderButton)
        {
            string retval = "";
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            // Note - need to set root folder to Desktop to correctly scroll 
            // to the initial directory.  This only works the second time this is opened.
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            if (Directory.Exists(initialDirectory))
            {
                fbd.SelectedPath = initialDirectory;
            }
            fbd.Description = dialogDescription;
            fbd.ShowNewFolderButton = showNewFolderButton;
            DialogResult dr = fbd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                retval = fbd.SelectedPath;
            }
            return retval;
        }

        /// <summary>
        /// This method will return the data contained within the specified stream.
        /// </summary>
        /// <param name="dataStream">the stream to read</param>
        /// <returns>byte[] with the contents of the stream</returns>
        public static byte[] GetBytes(Stream dataStream)
        {
            if (!dataStream.CanRead) { throw new ArgumentException(); }
            // This is optional         
            if (dataStream.CanSeek)
            {
                dataStream.Seek(0, SeekOrigin.Begin);
            }
            byte[] output = new byte[dataStream.Length];
            int bytesRead = dataStream.Read(output, 0, output.Length);

            return output;
        }

        /// <summary>
        /// This method returns an array of filenames that are in the provided directory that match the provided search pattern.  If the user desires,
        /// they can provide a search option (default is top directory only search), the alternative is SearchOption.AllDirectories which searches all
        /// sub-directories of the provided directory.
        /// </summary>
        /// <param name="directoryName">The full path name of the directory in which to search for files.</param>
        /// <param name="searchPattern">The search pattern to use to find files (e.g. "*.txt").</param>
        /// <param name="searchOption">The search option to use to find files - either Searchoption.TopDirectory which is the default
        /// or SearchOption.AllDirectories which searches all sub-directories as well as the top directory.</param>
        /// <returns>An array of files that match the search pattern in the provided directory using the provided search option.</returns>
        public static string[] GetFileNamesFromDirectoryNoDuplicates(string directoryName, string searchPattern, 
                                                                     SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            string[] getFilesResults = Directory.GetFiles(directoryName, searchPattern, searchOption);
            string[] retval = RemoveDuplicateStrings(getFilesResults);
            return retval;
        }

        /// <summary>
        /// This method returns an array of FileInfo objects that correspond to files in the provided directory that match the provided search pattern.  
        /// If the user desires, they can provide a search option (default is top directory only search), the alternative is SearchOption.AllDirectories 
        /// which searches all sub-directories of the provided directory.
        /// </summary>
        /// <param name="directoryName">The full path name of the directory in which to search for files.</param>
        /// <param name="searchPattern">The search pattern to use to find files (e.g. "*.txt").</param>
        /// <param name="searchOption">The search option to use to find files - either Searchoption.TopDirectory which is the default
        /// or SearchOption.AllDirectories which searches all sub-directories as well as the top directory.</param>
        /// <returns>An array of files that match the search pattern in the provided directory using the provided search option.</returns>
        public static FileInfo[] GetFileInfosFromDirectoryNoDuplicates(string directoryName, string searchPattern, 
                                                                       SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            DirectoryInfo dInfo = new DirectoryInfo(directoryName);
            FileInfo[] getFilesResults = dInfo.GetFiles(searchPattern, searchOption);
            FileInfo[] retval = RemoveDuplicateFileInfo(getFilesResults);
            return retval;
        }

        /// <summary>
        /// This method removes FileInfo objects that correspond to duplicate filenames from the list of FileInfo specified.
        /// DirectoryInfo, and Directory GetFiles can return duplicate entries in the results and this ensures these are removed.
        /// </summary>
        /// <param name="inputArray">The array of FileInfo objects to remove duplicates from.</param>
        /// <returns>The array of FileInfo objects with any duplicates removed.</returns>
        public static FileInfo[] RemoveDuplicateFileInfo(FileInfo[] inputArray)
        {
            FileInfo[] retVal = null;
            List<string> fnames = new List<string>();
            List<FileInfo> fInfos = new List<FileInfo>();
            if (inputArray != null)
            {
                foreach (FileInfo f in inputArray)
                {
                    if (!fnames.Contains(f.FullName))
                    {
                        fnames.Add(f.FullName);
                        fInfos.Add(f);
                    }
                }
                retVal = fInfos.ToArray();
            }
            return retVal;
        }

        /// <summary>
        /// This method removes duplicate strings from the input array of strings.
        /// </summary>
        /// <param name="inputArray">The array of strings from which to remove duplicates.</param>
        /// <returns>The array of strings with duplicates removed.</returns>
        public static string[] RemoveDuplicateStrings(string[] inputArray)
        {
            List<string> retVal = null;
            if (inputArray != null)
            {
                retVal = new List<string>();
                foreach (string s in inputArray)
                {
                    if (!retVal.Contains(s))
                    {
                        retVal.Add(s);
                    }
                }
            }
            return retVal.ToArray();
        }
    }
}
