﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

///////////////////////////////////////////////////////////////////////////////
//
//      Filename: OpenXmlExcelWriter.cs
//
//      (C) Copyright 2009 Biodesix Inc.
//      All Rights Reserved.
//
//      Revision History
//
//      Name            Date            Brief Description
//      -----------------------------------------------------------------------
//      Ed Rotthoff    July 23 2012   created this class to implement excel interface using Open XML instead of oledb interface.
//      Ed Rotthoff    Oct 22 2012    added logic to properly handle the auto creation of non existing xlsx files.
//      Ed Rotthoff    Oct 25 2012    added new method to append to an existing worksheet, the data table to be appended must
//                                    have columns that match the existing worksheet.
//      Ed Rotthoff   8/22/2016       check length of filenames including path, and worksheet names, notify the user if the filename is longer 
//                                    than 218 characters, notify the user if the worksheet name is longer than 31 chars.
//      Ed Rotthoff   8/25/2016       fix bug in append method, it would incorrectly append when columns match but are somehow scrambled in order.
///////////////////////////////////////////////////////////////////////////////
namespace Central.Util
{
    /// <summary>
    /// this class implements the methods used to write to Excel.
    /// the class can be used to write information to MS Excel. This class uses
    /// the OpenXML interface to excel, so there is no limitations to using this other than the
    /// excel file must be 2007 or a more recent version of Excel.
    /// the recommended use of this is to modify datatables within the code,then overwrite existing worksheets
    /// by first deleting them, then writing the datatable from application memory. Note, this approach will
    /// lose all formatting that might exist in the original worksheet, so users should be aware of this
    /// and not apply any additional formatting to the worksheets that are updated by the application.
    /// </summary>
    public class OpenXmlExcelWriter
    {
        ///<summary>
        /// object used to provide multi-threaded safety.
        /// </summary>
        private static object _syncRoot = new object();

        /// <summary>
        /// this method will append the specified data table to the specified worksheet in the 
        /// specified spreadsheet file, if the file and or worksheet do not exist they will be created.
        /// if the worksheet exists that is to be appended, the columns within the specified DataTable MUST 
        /// match one to one with the columns specified within the worksheet, 
        /// IE ALL of the existing columns must be used. if the columns in the specified data table do not
        /// match the pre-existing worksheet columns in number, an exception will be thrown. If the pre-existing
        /// worksheet columns and datatable columns match in number and any of the column names do not match
        /// exactly, read case sensitive, then an exception will be thrown. the columns must also match in the order of the
				/// column names, if the column names do not match in order an exception will be thrown.
        /// </summary>
        /// <param name="inputDataTable">the data table to append to the specified worksheet</param>
        /// <param name="fullyJustifiedSpreadsheetFilename">the spreadsheet to use</param>
        /// <param name="worksheetToCreate">the worksheet to append to, this will be created if it does not exist</param>
        public static void AppendDataTableToExcelWorksheet(DataTable inputDataTable,
           string fullyJustifiedSpreadsheetFilename,
           string worksheetToCreate)
        {

					OpenXmlExcelReader.CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					OpenXmlExcelReader.CheckLengthOFExcelWorksheet(worksheetToCreate);

            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            SpreadsheetDocument spreadsheetDocument = null;
            WorkbookPart workbookpart = null;
            try
            {
                if (!File.Exists(fullyJustifiedSpreadsheetFilename))
                {
                    string emptySS = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "Empty.xlsx";

                    lock (_syncRoot)
                    {
                        File.Copy(emptySS, fullyJustifiedSpreadsheetFilename);
                    }
                }
                spreadsheetDocument = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, true);
                workbookpart = spreadsheetDocument.WorkbookPart;

                bool newWorksheet = false;

                IEnumerable<Sheet> sheets = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(c => string.Compare(c.Name, worksheetToCreate) == 0);
                SheetData sheetData = null;
                if ((sheets == null) || (sheets.Count() == 0))//the worksheet does not exist, create it.
                {
                    // Add a WorksheetPart to the WorkbookPart.
                    WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet(new SheetData());

                    // Add Sheets to the Workbook.
                    Sheets tsheets = spreadsheetDocument.WorkbookPart.Workbook.Sheets;

                    // Append a new worksheet and associate it with the workbook.
                    Sheet sheet = new Sheet();

                    uint theSheetId = 1;

                    if (tsheets.Elements<Sheet>().Count() > 0)
                    {
                        theSheetId =
                            tsheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }


                    sheet.Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart);
                    sheet.SheetId = theSheetId;
                    sheet.Name = worksheetToCreate;

                    tsheets.Append(sheet);

                    sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                    newWorksheet = true;

                }
                else // the worksheet existed before.
                {
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    sheetData = workSheet.GetFirstChild<SheetData>();
                }

              
                Row r = null;
                int dcCount = 1;
                List<string> theColumnHeaders = new List<string>();
                int rCount = 2;

                if (newWorksheet)
                {//write the header row
                    r = new Row();
                    foreach (DataColumn dc in inputDataTable.Columns)
                    {
                        string valueT = dc.ColumnName;
                        //Create a new inline string cell.
                        Cell c = new Cell();
                        c.DataType = CellValues.InlineString;
                        //Add text to the text cell.
                        InlineString inlineString = new InlineString();
                        Text t = new Text();
                        t.Text = valueT;
                        inlineString.AppendChild(t);
                        c.AppendChild(inlineString);
                        r.Append(c);
                        theColumnHeaders.Add(GetExcelColumnName(dcCount));
                        c.CellReference = theColumnHeaders[dcCount - 1] + "1";
                        c.CellValue = new CellValue(valueT);
                        dcCount++;
                    }
                    sheetData.Append(r);
                }
                else //if worksheet existed just note the columns to be used.
                {
                    
                    foreach (DataColumn dc in inputDataTable.Columns)
                    {
                        theColumnHeaders.Add(GetExcelColumnName(dcCount));
                        dcCount++;
                    }
                    
                }
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                List<string> theNames = new List<string>();
                foreach (Cell cell in rows.ElementAt(0))
                {
                    string theValue = OpenXmlExcelReader.GetCellValue(spreadsheetDocument, cell);
                    if (theValue.Length > 0)
                    {
                        theNames.Add(theValue);
                    }
                }
                if (theNames.Count != theColumnHeaders.Count)
                {
                    throw new Exception(" append to existing worksheet error, the number of columns to be appended does not match the columns defined " +
                        " in the spreadsheet " + fullyJustifiedSpreadsheetFilename + " in worksheet " + worksheetToCreate);
                }

							  //check that the name and order of names of columns matches.
								int countem = 0;
                foreach (DataColumn dc in inputDataTable.Columns)
                {
                    if (!theNames[countem].Equals(dc.ColumnName))
                    {
                        throw new Exception(" append to existing worksheet error, the columns to be appended do not match the columns defined " +
                        " in the spreadsheet " + fullyJustifiedSpreadsheetFilename + " in worksheet " + worksheetToCreate + " for column "+ dc.ColumnName.ToString()+" please check that the order, and the names of the columns match.");
                    }
										countem++;
                }
                rCount = rows.Count<Row>() + 1;
                
                foreach (DataRow dr in inputDataTable.Rows)
                {
                    r = new Row();
                    dcCount = 0;
                    foreach (DataColumn dc in inputDataTable.Columns)
                    {
                        string valueT = dr[dc.ColumnName].ToString();
                        //Create a new inline string cell.
                        Cell c = new Cell();
                        c.DataType = CellValues.InlineString;
                        //Add text to the text cell.
                        InlineString inlineString = new InlineString();
                        Text t = new Text();
                        t.Text = valueT;
                        inlineString.AppendChild(t);
                        c.AppendChild(inlineString);
                        c.CellReference = theColumnHeaders[dcCount] + rCount.ToString("#######");
                        c.CellValue = new CellValue(valueT);
                        r.Append(c);
                        dcCount++;
                    }
                    rCount++;
                    sheetData.Append(r);
                }

                workbookpart.Workbook.Save();
            }
            finally
            {
                if (spreadsheetDocument != null)
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
            }
        }


        /// <summary>
        /// this method will write the specified data table to the specified worksheet in the specified spreadsheet.
        /// if the worksheet exists an Exception will be thrown, use OpenXMLExcelReader.WorksheetExists to check.
        /// </summary>
        /// <param name="inputDataTable">the data table to write</param>
        /// <param name="fullyJustifiedSpreadsheetFilename">the spreadsheet filename.</param>
        /// <param name="worksheetToCreate">the worksheet to create</param>
        public static void WriteDataTableToExcelWorksheet(DataTable inputDataTable,
            string fullyJustifiedSpreadsheetFilename,
            string worksheetToCreate)
        {

					OpenXmlExcelReader.CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					OpenXmlExcelReader.CheckLengthOFExcelWorksheet(worksheetToCreate);

            if (File.Exists(fullyJustifiedSpreadsheetFilename))
            {
                //check if the worksheet exists. if it does throw an error.
                if (Central.Util.OpenXmlExcelReader.WorksheetExists(fullyJustifiedSpreadsheetFilename, worksheetToCreate))
                {
                    throw new Exception("The worksheet " + worksheetToCreate + " already exists in the file " + fullyJustifiedSpreadsheetFilename);
                }
            }

            // Create a spreadsheet document by supplying the filepath.
            // By default, AutoSave = true, Editable = true, and Type = xlsx.
            SpreadsheetDocument spreadsheetDocument = null;
            WorkbookPart workbookpart = null;
            try
            {
                if (!File.Exists(fullyJustifiedSpreadsheetFilename))
                {
                    string emptySS = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "Empty.xlsx";
                    
                    lock (_syncRoot)
                    {
                        File.Copy(emptySS, fullyJustifiedSpreadsheetFilename);
                    }
                }
                spreadsheetDocument = SpreadsheetDocument.Open(fullyJustifiedSpreadsheetFilename, true);
                workbookpart = spreadsheetDocument.WorkbookPart;

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.Sheets;

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet();

                uint theSheetId = 1;

                if (sheets.Elements<Sheet>().Count() > 0)
                {
                    theSheetId =
                        sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }


                sheet.Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart);
                sheet.SheetId = theSheetId;
                sheet.Name = worksheetToCreate;

                sheets.Append(sheet);

                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                //write the header row.
                Row r = new Row();
                List<string> theColumnHeaders = new List<string>();
                int dcCount = 1;
                foreach (DataColumn dc in inputDataTable.Columns)
                {
                    string valueT = dc.ColumnName;
                    //Create a new inline string cell.
                    Cell c = new Cell();
                    c.DataType = CellValues.InlineString;
                    //Add text to the text cell.
                    InlineString inlineString = new InlineString();
                    Text t = new Text();
                    t.Text = valueT;
                    inlineString.AppendChild(t);
                    c.AppendChild(inlineString);
                    r.Append(c);
                    theColumnHeaders.Add(GetExcelColumnName(dcCount));
                    c.CellReference = theColumnHeaders[dcCount - 1] + "1";
                    c.CellValue = new CellValue(valueT);
                    dcCount++;
                }
                sheetData.Append(r);
                int rCount = 2;
                foreach (DataRow dr in inputDataTable.Rows)
                {
                    r = new Row();
                    dcCount = 0;
                    foreach (DataColumn dc in inputDataTable.Columns)
                    {
                        string valueT = dr[dc.ColumnName].ToString();
                        //Create a new inline string cell.
                        Cell c = new Cell();
                        c.DataType = CellValues.InlineString;
                        //Add text to the text cell.
                        InlineString inlineString = new InlineString();
                        Text t = new Text();
                        t.Text = valueT;
                        inlineString.AppendChild(t);
                        c.AppendChild(inlineString);
                        c.CellReference = theColumnHeaders[dcCount]+rCount.ToString("#######");
                        c.CellValue = new CellValue(valueT);
                        r.Append(c);
                        dcCount++;
                    }
                    rCount++;
                    sheetData.Append(r);
                }

                workbookpart.Workbook.Save();
            }
            finally
            {
                if (spreadsheetDocument != null)
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
            }
        }

        /// <summary>
        /// this method will overwrite an existing worksheet with the contents of the specified datatable,
        /// during the overwrite a temporary version of the 
        /// original file is made, if any failures occur then the original file is put back into place.
        /// in order to update a worksheet with a Spreadsheet, read it, then update in the code, the overwrite it.
        /// </summary>
        /// <param name="theDT">the data table to use in overwrite.</param>
        /// <param name="fullyJustifiedSpreadsheetFilename">the spreadsheet filename</param>
        /// <param name="worksheetname">the worksheet name to overwrite</param>
        public static void OverWriteWorksheet(DataTable theDT, string fullyJustifiedSpreadsheetFilename, string worksheetname)
        {
					OpenXmlExcelReader.CheckLengthOFExcelFilename(fullyJustifiedSpreadsheetFilename);
					OpenXmlExcelReader.CheckLengthOFExcelWorksheet(worksheetname);

            try
            {
                if (File.Exists(fullyJustifiedSpreadsheetFilename))
                {
                    File.Copy(fullyJustifiedSpreadsheetFilename, fullyJustifiedSpreadsheetFilename + "~");
                    if (Central.Util.OpenXmlExcelReader.WorksheetExists(fullyJustifiedSpreadsheetFilename, worksheetname))
                    {
                        Central.Util.OpenXmlExcelWriter.DeleteWorkSheet(fullyJustifiedSpreadsheetFilename, worksheetname);
                    }
                }
                Central.Util.OpenXmlExcelWriter.WriteDataTableToExcelWorksheet(theDT, fullyJustifiedSpreadsheetFilename, worksheetname);
                
            }
            catch (Exception ex)
            {
                if (File.Exists(fullyJustifiedSpreadsheetFilename + "~"))
                {
                    File.Copy(fullyJustifiedSpreadsheetFilename + "~", fullyJustifiedSpreadsheetFilename);
                }
                throw (ex);
            }
            finally
            {
                File.Delete(fullyJustifiedSpreadsheetFilename + "~");
            }
        }

       

        /// <summary>
        /// this method will remove the specified worksheet from the specified spreadsheet, it is recommended that you 
        /// surround calls to this method with logic to ensure that the resulting file is in the proper state.
        /// </summary>
        /// <param name="fileName">the fully justified filename of the spreadsheet</param>
        /// <param name="sheetToDelete">the worksheet to remove.</param>
        public static void DeleteWorkSheet(string fileName, string sheetToDelete)
        {
					OpenXmlExcelReader.CheckLengthOFExcelFilename(fileName);
					OpenXmlExcelReader.CheckLengthOFExcelWorksheet(sheetToDelete);

            string Sheetid = "";
            WorkbookPart wbPart = null;
						//check if the worksheet exists. if it does throw an error.
						if (!(Central.Util.OpenXmlExcelReader.WorksheetExists(fileName, sheetToDelete)))
						{
							throw new Exception("The worksheet " + sheetToDelete + " does not exists in the file " + fileName);
						}
            //Open the workbook
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
            {
                try
                {
									
                    wbPart = document.WorkbookPart;

                    // Get the pivot Table Parts
                    IEnumerable<PivotTableCacheDefinitionPart> pvtTableCacheParts = wbPart.PivotTableCacheDefinitionParts;
                    Dictionary<PivotTableCacheDefinitionPart, string> pvtTableCacheDefinationPart = new Dictionary<PivotTableCacheDefinitionPart, string>();
                    foreach (PivotTableCacheDefinitionPart Item in pvtTableCacheParts)
                    {
                        PivotCacheDefinition pvtCacheDef = Item.PivotCacheDefinition;
                        //Check if this CacheSource is linked to SheetToDelete
                        var pvtCahce = pvtCacheDef.Descendants<CacheSource>().Where(s => s.WorksheetSource.Sheet == sheetToDelete);
                        if (pvtCahce.Count() > 0)
                        {
                            pvtTableCacheDefinationPart.Add(Item, Item.ToString());
                        }
                    }
                    foreach (var Item in pvtTableCacheDefinationPart)
                    {
                        wbPart.DeletePart(Item.Key);
                    }
                    //Get the SheetToDelete from workbook.xml
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetToDelete).FirstOrDefault();
                    if (theSheet == null)
                    {
                        // The specified sheet doesn't exist.
                    }
                    //Store the SheetID for the reference
                    Sheetid = theSheet.SheetId;

                    // Remove the sheet reference from the workbook.
                    WorksheetPart worksheetPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    theSheet.Remove();

                    // Delete the worksheet part.
                    wbPart.DeletePart(worksheetPart);

                    //Get the DefinedNames
                    var definedNames = wbPart.Workbook.Descendants<DefinedNames>().FirstOrDefault();
                    if (definedNames != null)
                    {
                        foreach (DefinedName Item in definedNames)
                        {
                            // This condition checks to delete only those names which are part of Sheet in question
                            if (Item.Text.Contains(sheetToDelete + "!"))
                                Item.Remove();
                        }
                    }
                    // Get the CalculationChainPart 
                    //Note: An instance of this part type contains an ordered set of references to all cells in all worksheets in the 
                    //workbook whose value is calculated from any formula

                    CalculationChainPart calChainPart;
                    calChainPart = wbPart.CalculationChainPart;
                    if (calChainPart != null)
                    {
                        var calChainEntries = calChainPart.CalculationChain.Descendants<CalculationCell>().Where(c => c.SheetId == Sheetid);
                        foreach (CalculationCell Item in calChainEntries)
                        {
                            Item.Remove();
                        }
                        if (calChainPart.CalculationChain.Count() == 0)
                        {
                            wbPart.DeletePart(calChainPart);
                        }
                    }
                }
                finally
                {
                    if (wbPart != null)
                    {
                        // Save the workbook.
                        wbPart.Workbook.Save();
                    }
                    if (document != null)
                    {
                        document.Close();
                    }
                }
            }
        }

        /// <summary>
        /// this method will generate the Excel centric column name for columns 1-N.
        /// </summary>
        /// <param name="columnNumber">column number 1 -N</param>
        /// <returns>the excel centric column name A,B...Z, AA,BB...ZZ etc</returns>
        private static string GetExcelColumnName(int columnNumber) 
        { 
            int dividend = columnNumber; 
            string columnName = String.Empty; 
            int modulo; 
            while (dividend > 0) 
            { 
                modulo = (dividend - 1) % 26; 
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }
            return columnName; 
        } 

    }
}
