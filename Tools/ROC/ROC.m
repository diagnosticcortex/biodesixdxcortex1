%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Description:                                                            
%-------------------------------------------------------------------------
%This function loads the pre-processed spreadsheets, created by 
%preProcessROCtables.m and calculates the ROC curve of the corresponding 
%classifier and respective error bars
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
%-------------------------------------------------------------------------
%DIRECTORY - string with the path to the directory where the pre-processed
%spreadsheets are located
%SET - string identifying what kind of data set we are dealing with
%    - possible values for SET: 'Development' | 'Validation'
%CUTOFF - user provided probablility threshod for which sensitivity and
%specificity should be calculated and plotted
%NBOOT - # of boot strap realizations of the probabilities (for each
%samples) to be generated, in order to determine the error bars
%ALPHA - (1-ALPHA) is the confidence level considered to determine the
%confidence interval of the sensitivity / specificity values
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outputs:                                                                
%-------------------------------------------------------------------------
%SENS - vector with calculated sensitivities. The size of SENS is Nx1 where
%N is the number of scanned probability thresholds
%SENS_CI - 2xN array. The 1st row contains the lower limits for the CI
%and the 2nd row the upper limits
%SPEC - Nx1 vector with calculated specificities
%SPEC_CI - 2xN array. The 1st row contains the lower limits for the CI
%and the 2nd row the upper limits
%PTHRE - list of scanned probability thresholds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Intended Use:
%-------------------------------------------------------------------------
%Create ROC curves
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Dependencies:
%-------------------------------------------------------------------------
%Analysis\ROC\accuracy.m
%Analysis\ROC\sensitivity.m
%Analysis\ROC\specificity.m
function [sens,sens_lo,sens_up,spec,spec_lo,spec_up,pthre] = ROC(directory,set,cutoff,Nboot,alpha)

close all
clc

%load probabilities and definitions:
[P,filename_P,raw] = xlsread([directory '\P.xlsx']);
Def = P(:,1); %Definition. 1 Cancer. 0 NoCancer
P = P(:,2:end); %Master Classifier Probabilities

%If dealing with a development set, load boolean with info about when the
%sample was in the training set:
if strcmp(set,'Development')
    [T,filename_T,raw] = xlsread([directory '\T.xlsx']); %1s where the sample was in the test set
elseif ~strcmp(set,'Validation')
    error('The data set is not recognized')
end

n_samples = size(P,1); %# of samples
n_MCs = size(P,2); %# of Master Classifiers

%Check if the filenames coming from P.xlsx and T.xlsx are the same and in
%the same order:
if strcmp(set,'Development')
    for i=1:n_samples
        if strcmp(filename_P{i},filename_T{i}) ~= 1
            error('The filenames or their order are not the same in T.xlsx and P.xlsx')
        end
    end
end

%Calculate probability averages and standard deviations
p_avg = zeros(n_samples,1);
std_dev = zeros(n_samples,1);
for i=1:n_samples
    if strcmp(set,'Development')
        index = T(i,:)==1; %use only p's given for MCs that had the sample in the test set
    else %Validation
        index = logical(ones(size(P(i,:)))); %Use p's from all MCs
    end
    p_avg(i) = mean(P(i,index));
    std_dev(i) = std(P(i,index));
end

%Create list of probability thresholds to scan:
pthre = sort(p_avg);
%Remove values of p too close to each other:
i = 2;
while i<=length(pthre)
    if (pthre(i)-pthre(i-1))<1e-8 & i==length(pthre)
        pthre(i-1) = [];
    elseif (pthre(i)-pthre(i-1))<1e-8
        pthre(i) = [];
    else
        i=i+1;
    end
end
%pthre = pthre(abs(diff(pthre)) > 1e-8);
%Calculate sensitivities and specificties for the list of p thresholds:
[sens] = sensitivity(p_avg,pthre,Def);
[spec] = specificity(p_avg,pthre,Def);

%create average probabilities from boot strapping over the MCs, for each sample:
p = zeros(n_samples,Nboot);
for i = 1:n_samples
    if strcmp(set,'Development')
        index = T(i,:)==1;
    else
        index = logical(ones(size(P(i,:))));
    end
    [p(i,:)] = bootstrp(Nboot,'mean',P(i,index))';
    disp(['Creating boot strap probability realizatons for Sample #: ' num2str(i)])

end

%Calculate senstivities and specificities for the different boot strap
%realizations:
sens_ = zeros(length(pthre),Nboot);
spec_ = zeros(length(pthre),Nboot);
for j = 1:Nboot
    sens_(:,j) = sensitivity(p(:,j),pthre,Def);
    spec_(:,j) = specificity(p(:,j),pthre,Def);
end
%Calculate percentiles, for each p threshold, based on the desired
%confidence level:
sens_ci = prctile(sens_',[100*alpha/2,100*(1-alpha/2)]);
spec_ci = prctile(spec_',[100*alpha/2,100*(1-alpha/2)]);

sens_up = sens_ci(2,:);
spec_up = spec_ci(2,:);
sens_lo = sens_ci(1,:);
spec_lo = spec_ci(1,:);

%plot ROC curve
figure(1)
hold off
area(1-spec_ci(2,:),sens_ci(2,:),'FaceColor','c','EdgeColor','none')
hold on
area(1-spec_ci(1,:),sens_ci(1,:),'FaceColor',[1 1 1],'EdgeColor','none')
plot(1-spec,sens,'.r')
plot([0 1],[0 1],'b')
xlabel('1 - Specificity')
ylabel('Sensitivity')

%Scan the p thresholds given by the user and calculate sensitivity and
%specificity, also save into a file the labels given by such thresholds
Def_str = cell(n_samples,1);
Class_str = cell(n_samples,length(cutoff));
cuttoff_columnNames = cell(1,length(cutoff));
for j = 1: length(cutoff)
    for i = 1:n_samples
       switch Def(i)
           case 0
               Def_str{i} = 'NoCancer';
           case 1
               Def_str{i} = 'Cancer';
           otherwise
               error('The value of definition is not recognized.')
       end
       %Determine classification labels based on the cutoff
       if p_avg(i) < cutoff(j)
           Class_str{i,j} = 'Cancer';
       else
           Class_str{i,j} = 'NoCancer';
       end
    end
    
    %calculate errors of sensitivity and specificity
    sens_ = zeros(Nboot,1);
    spec_ = zeros(Nboot,1);
    for i = 1:Nboot
        sens_(i) = sensitivity(p(:,i),cutoff(j),Def);
        spec_(i) = specificity(p(:,i),cutoff(j),Def);
    end
    sens_ci_ = prctile(sens_,[100*alpha/2,100*(1-alpha/2)]);
    spec_ci_ = prctile(spec_,[100*alpha/2,100*(1-alpha/2)]);
    
    
    %calculate nominal values for sensitivity and specificity
    [sens_] = sensitivity(p_avg,cutoff(j),Def);
    [spec_] = specificity(p_avg,cutoff(j),Def);
    [accu_] = accuracy(p_avg,cutoff(j),Def);
    
    %Superimpose point in the ROC curve: 
    plot(1-spec_,sens_,'ob')
    
    disp(['cutoff: ' num2str(cutoff(j)) ' | Overall accuracy: ' num2str(accu_) ...
        ' | Sensitivity: ' num2str(sens_) ' with ' num2str(100*(1-alpha)) '% CI=[' num2str(sens_ci_(1)) ',' num2str(sens_ci_(2)) ... 
        '] | Specificity: ' num2str(spec_) ' with ' num2str(100*(1-alpha)) '% CI=[' num2str(spec_ci_(1)) ',' num2str(spec_ci_(2)) ']'])
    cutoff_columnNames{j} = ['Class_Label_Cutoff_' num2str(cutoff(j))];
end

%save averaged probabilities in a xlsx file:
delete([directory '\AveragedProbabilities.xlsx'])
xlswrite([directory '\AveragedProbabilities.xlsx'],[{'Filename','Definition','p_mean','p_std'} cutoff_columnNames; filename_P Def_str num2cell(p_avg) num2cell(std_dev) Class_str])

%save ROC metrics in a xlsx file:
delete([directory '\ROC_output.xlsx'])
xlswrite([directory '\ROC_output.xlsx'],[{'p_thre','Sensitivity',['SensLowLimit' num2str(100*(1-alpha)) 'CI'],['SensUpLimit' num2str(100*(1-alpha)) 'CI'],'Specificity',['SpecLowLimit' num2str(100*(1-alpha)) 'CI'],['SpecUpLimit' num2str(100*(1-alpha)) 'CI']};num2cell([pthre,sens, sens_ci(1,:)',sens_ci(2,:)',spec, spec_ci(1,:)',spec_ci(2,:)'])]);

%check if points (0,0) and (1,1) are part of the curves obtained from the
%boostraps:
if sens_up(end)~=1.0 | spec_up(end)~=0.0
    sens_up = [sens_up 1.0];
    spec_up = [spec_up 0.0];
end
if sens_up(end)~=0.0 | spec_up(end)~=1.0
    sens_up = [0.0 sens_up];
    spec_up = [1.0 spec_up];
end
if sens_lo(end)~=1.0 | spec_lo(end)~=0.0
    sens_lo = [sens_lo 1.0];
    spec_lo = [spec_lo 0.0];
end
if sens_lo(end)~=0.0 | spec_lo(end)~=1.0
    sens_lo = [0.0 sens_lo];
    spec_lo = [1.0 spec_lo];
end
%check if point (0,0) is part of the curve obtained from the
%actual data:
if sens(end)~=0.0 | spec(end)~=1.0
    sens = [0.0; sens];
    spec = [1.0; spec];
end
%calculate are under the curve and respective errors
auc = trapz(1-spec,sens);
auc_max = trapz(1-spec_up,sens_up);
auc_min = trapz(1-spec_lo,sens_lo);
disp(['AUC = ' num2str(auc) ' with ' num2str(100*(1-alpha)) '% CI=[' num2str(auc_min) ',' num2str(auc_max) ']'])
end



