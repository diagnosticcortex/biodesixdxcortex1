%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Description:                                                            
%-------------------------------------------------------------------------
%This function takes the probabilities given by a classifier, the learning
%labels and a list of probability thresholds to scan and outputs a list of
%overall accuracies calculated for each of the given probability thresholds.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
%-------------------------------------------------------------------------
%P - vector containing the classifier probabilities for N_samples
%THR - list of probability N thresholds to scan
%Def - learning labels of the N_samples "1" is positive.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outputs:                                                                
%-------------------------------------------------------------------------
%ACCU - vector with calculatedaccuracies. The size of ACCU is Nx1 where
%N is the number of scanned probability thresholds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Intended Use:
%-------------------------------------------------------------------------
%Calculate performance metric
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Dependencies:
%-------------------------------------------------------------------------
%No dependencies
function [accu] = accuracy(p,thr,def)
    N = length(thr);
    nfn  = zeros(N,1); % # of false negatives
    ntn  = zeros(N,1); % # of true negatives
    nfp  = zeros(N,1); % # of false positives
    ntp  = zeros(N,1); % # of true positives
    for i = 1:N
        nfn(i) = sum(p > thr(i) & def == 1);
        ntn(i) = sum(p > thr(i) & def == 0);
        nfp(i) = sum(p <= thr(i) & def == 0);
        ntp(i) = sum(p <= thr(i) & def == 1);
    end
   accu = (ntp+ntn) ./ (ntp + nfn + ntn + nfp);
end