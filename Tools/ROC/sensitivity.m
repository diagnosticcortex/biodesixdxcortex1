%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Description:                                                            
%-------------------------------------------------------------------------
%This function takes the probabilities given by a classifier, the learning
%labels and a list of probability thresholds to scan and outputs a list of
%sensitivities calculated for each of the given probability thresholds.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
%-------------------------------------------------------------------------
%P - vector containing the classifier probabilities for N_samples
%THR - list of probability N thresholds to scan
%Def - learning labels of the N_samples "1" is positive.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outputs:                                                                
%-------------------------------------------------------------------------
%SENS - vector with calculated sensitivities. The size of SENS is Nx1 where
%N is the number of scanned probability thresholds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Intended Use:
%-------------------------------------------------------------------------
%Calculate performance metric
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Dependencies:
%-------------------------------------------------------------------------
%No dependencies
function [sens] = sensitivity(p,thr,def)
    N = length(thr);
    nfn  = zeros(N,1); % # of false negatives
    ntp  = zeros(N,1); % # of true positives
    for i = 1:N
        nfn(i) = sum(p > thr(i) & def == 1);
        ntp(i) = sum(p <= thr(i) & def == 1);
    end
    sens = ntp ./(ntp + nfn);
end