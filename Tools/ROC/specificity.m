%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Description:                                                            
%-------------------------------------------------------------------------
%This function takes the probabilities given by a classifier, the learning
%labels and a list of probability thresholds to scan and outputs a list of
%specificities calculated for each of the given probability thresholds.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
%-------------------------------------------------------------------------
%P - vector containing the classifier probabilities for N_samples
%THR - list of probability N thresholds to scan
%Def - learning labels of the N_samples "1" is positive.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outputs:                                                                
%-------------------------------------------------------------------------
%SPEC - vector with calculated specificities. The size of SPEC is Nx1 where
%N is the number of scanned probability thresholds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Intended Use:
%-------------------------------------------------------------------------
%Calculate performance metric
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Dependencies:
%-------------------------------------------------------------------------
%No dependencies
function [spec] = specificity(p,thr,def)
    N = length(thr);
    ntn  = zeros(N,1); % # of true negatives
    nfp  = zeros(N,1); % # of false positives
    for i = 1:N
        ntn(i) = sum(p > thr(i) & def == 0);
        nfp(i) = sum(p <= thr(i) & def == 0);
    end
   spec = ntn ./ (ntn + nfp);
end